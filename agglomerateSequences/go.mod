module gitlab.com/Grouumf/processfastafiles/agglomerateSequences

go 1.15

require (
	github.com/biogo/hts v1.4.3 // indirect
	github.com/biogo/store v0.0.0-20201120204734-aad293a2328f // indirect
	github.com/jinzhu/copier v0.3.2 // indirect
	github.com/klauspost/pgzip v1.2.5 // indirect
	gitlab.com/Grouumf/ATACdemultiplex v0.48.2
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20210802220835-4b20ff939e6b
)
