package main

import (
	"flag"
	"fmt"
	"os"
	utils "gitlab.com/Grouumf/processfastafiles/fastaUtils"
	fileutils "gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils"
	"sync"
	"time"
	"io"
	"path"
)


/*GENOMEFOLDER path containing fastq file */
var GENOMEFOLDER utils.Foldername

/*BLASTRESULTFOLDER result for blast file in format 7*/
var BLASTRESULTFOLDER utils.Foldername

/*REGEXFOLDER regex to capture specific genomes */
var REGEXFOLDER string

/*OUTFILE out */
var OUTFILE string

/*MUTEX ...*/
var MUTEX sync.Mutex

/*NBTHREADS ... */
var NBTHREADS int

/*NBSEQ total number of seq */
var NBSEQ int

/*SAVEGENOMENAME save genome name in title */
var SAVEGENOMENAME bool

/*WRITEMATCH write match */
var WRITEMATCH bool

func main() {
		flag.Usage = func() {
			fmt.Fprintf(
				os.Stderr, `
################ Module to prepare sequence ################################
Create a DB of fasta sequences into one output file according to sequence blast hits (output format type '7' required)

USAGE:
agglomerateSequences -genomes <folder> -blastResults <folder> -out <file> (optional -regex <str> -threads <int>  -write_hit -write_genome)
############################################################################

`)
			flag.PrintDefaults()
		}

	flag.Var(&BLASTRESULTFOLDER, "res",
		"name of the folder having blast type results. each results should be in format 7")

	flag.Var(&GENOMEFOLDER, "genomes",
		"name of the folder containing fastq results")

	flag.StringVar(&REGEXFOLDER, "regex", "",
		"optional regex to capture only specific folders")

	flag.BoolVar(&WRITEMATCH, "write_hit", false,
		"add hit ID with seq ID")

	flag.IntVar(&NBTHREADS, "threads", 4,
		"nb treads to use")

	flag.BoolVar(&SAVEGENOMENAME, "write_genome", false,
		"Add genome name in seq ID (split after second _ )")

	flag.StringVar(&OUTFILE, "out", "",
		"optional regex to capture only specific folders")

	flag.Parse()

	if OUTFILE == "" {
		panic("-out should not be empty")
	}

	fmt.Printf("scanning folders...\n")
	waiting := sync.WaitGroup{}
	waiting.Add(2)

	go GENOMEFOLDER.ListFiles(REGEXFOLDER, &waiting)
	go BLASTRESULTFOLDER.ListFiles(REGEXFOLDER, &waiting)

	tStart := time.Now()
	waiting.Wait()
	tDiff := time.Since(tStart)
	fmt.Printf("Scanning done in time: %f s \n", tDiff.Seconds())

	writer := fileutils.ReturnWriter(OUTFILE)
	defer fileutils.CloseFile(writer)

	guard := make(chan int, NBTHREADS)

	count := 0
	nbFiles := len(GENOMEFOLDER.GetFileNames())

	tStart = time.Now()
	fmt.Printf("Number of files processed: %d\n", nbFiles)
	for _, fastaFile := range GENOMEFOLDER.GetFileNames() {
		if REGEXFOLDER != "" {
			fmt.Printf("File found: %s\n", fastaFile)
		}

		blastFile, isFound := BLASTRESULTFOLDER.GetFilePath(fastaFile)
		fastaFile = path.Join(GENOMEFOLDER.String(), fastaFile)

		if !isFound {
			fmt.Printf("Genome file: %s not found in results\n", fastaFile)
			continue
		}
		guard <- count

		waiting.Add(1)
		go processOneFastaFile(
			fastaFile, blastFile, &writer, &waiting, guard)
		count++
		fmt.Printf("\rNb files processed: %d / %d ", count, nbFiles)
	}
	waiting.Wait()

	tDiff = time.Since(tStart)
	fmt.Printf("\nFasta file done in time: %f s \n", tDiff.Seconds())
	fmt.Printf("output fasta file created: %s with %d sequences\n", OUTFILE, NBSEQ)
}

func processOneFastaFile(
	fastaFile,
	blastFile string,
	writer *io.WriteCloser,
	waiting * sync.WaitGroup, guard chan int) {
	defer waiting.Done()
	fasta := utils.FastaFile{}
	blast := utils.BlastResultFmt7{}

	var err error

	fasta.Init(fastaFile)
	defer fasta.Close()

	blast.Init(blastFile)
	defer blast.Close()

	if WRITEMATCH {
		fasta.WriteMatchingSeqIDWithMatchToBuff(
			blast.GetAllSeqIDsWithMatch(), SAVEGENOMENAME, false)
	} else {
		fasta.WriteMatchingSeqIDToBuff(
			blast.GetAllSeqIDs(), SAVEGENOMENAME, false)
	}

	MUTEX.Lock()
	_, err = (*writer).Write(fasta.WriteBuff())
	fileutils.Check(err)
	NBSEQ += blast.GetNbSeqs()
	MUTEX.Unlock()
	<-guard
}
