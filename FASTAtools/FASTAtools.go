package main

import (
	"flag"
	"fmt"
	utils "gitlab.com/Grouumf/processfastafiles/fastaUtils"
	fileutils "gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils"
	"time"
	"os"
	"io"
	"path/filepath"
	"path"
	"strings"
	"sync"
	"math/rand"
	"sort"
	"bytes"
	"strconv"
)


/*PATTERN matching string pattern within sequences*/
var PATTERN string

/*EXTRACT extract fasta based on sequence pattern matching */
var EXTRACT bool

/*INPUTFAA input fasta file */
var INPUTFAA fileutils.ArrayFlags

/*PREFIX output prefix */
var PREFIX string

/*OUTPATH output path */
var OUTPATH string

/*OUTFILE output file */
var OUTFILE string

/*OUTFILE2 output file nb 2 */
var OUTFILE2 string

/*NBSPLIT number of split */
var NBSPLIT int

/*SPLIT split fasta */
var SPLIT bool

/*CLEAN clean fasta */
var CLEAN bool

/*SORT sort fasta */
var SORT bool

/*COUNT count fasta */
var COUNT bool

/*UNIQUE unique fasta */
var UNIQUE bool

/*SUBSET count fasta */
var SUBSET bool

/*SIZE size fasta */
var SIZE bool

/*MAXPERCAT ...*/
var MAXPERCAT int

/*TRIM ...*/
var TRIM bool

/*SELECT ...*/
var SELECT bool

/*TAGSEQ ...*/
var TAGSEQ bool

/*TAGEXTRACT extract fasta sequences per taag*/
var TAGEXTRACT bool

/*CATPOS ...*/
var CATPOS int

/*CATSEP ...*/
var CATSEP string

/*NBTHREADS ...*/
var NBTHREADS int

/*MUTEX ...*/
var MUTEX sync.Mutex

/*WAITING ...*/
var WAITING sync.WaitGroup

/*SELECTEDSEQ ...*/
var SELECTEDSEQ map[string]bool

/*SELECTEDSEQWITHTAG ...*/
var SELECTEDSEQWITHTAG map[string]string

/*SEQSUBSETFILENAME file name*/
var SEQSUBSETFILENAME fileutils.Filename


func main() {

	flag.Usage = func() {
		fmt.Fprintf(
			os.Stderr, `
################ Module to split Fasta seq ################################
FASTA tools

USAGE:
# split fasta in multiple fasta with a defined number of sequences for each
FASTAtools -split -fa <fasta file> -n <number> -prefix <string>

# clean fasta
FASTAtools -clean -fa <fasta file> -out <file> (-fa <fasta file2))

# rm dupplicates from fasta
FASTAtools -unique -fa <fasta file> -out <file> (-fa <fasta file2) -trim -category_pos <string> -category_sep <string>)

# count fasta
FASTAtools -count -fa <fasta file> (-fa <fasta file2>)

# extract fasta based on sequence pattern matching
FASTAtools -extract -fa <fasta file> -pattern <string> (-fa <fasta file 2>... -out <matching seq file> -neq (non matching seq file> -trim)

# get fasta seq size
FASTAtools -size -fa <fasta file> (-fa <fasta file2> -out <file>)

# select a subet of fasta using a list of input FASTA ID.
FASTAtools -select -fa <fasta file> -fastaID_file <file> (-out <file> -tagSeq -tagExtract)

# Sort fasta by size
FASTAtools -sort -fa <fasta file> -out <file> (-trim)

# subset fasta per category
FASTAtools -subset -fa <fasta file> -out <file> (-category_max <int> -category_sep <string> -category_pos <int> -nbtreads)
############################################################################

`)
		flag.PrintDefaults()
		}

	flag.Var(&INPUTFAA, "fa", "Input fasta file(s)")
	flag.StringVar(&PREFIX, "prefix", "tmp", "prefix for output file")
	flag.StringVar(&OUTPATH, "dir", "", "output directory (default current)")
	flag.StringVar(&OUTFILE, "out", "", "output file")
	flag.StringVar(&OUTFILE2, "neq", "", "output file for non matching sequences")
	flag.StringVar(&PATTERN, "pattern", "", "matching string pattern within sequences")
	flag.Var(&SEQSUBSETFILENAME, "fastaID_file", "file of fasta ID to select")
	flag.BoolVar(&SPLIT, "split", false, "split fasta")
	flag.BoolVar(&EXTRACT, "extract", false, "extract fasta based on sequence pattern matching")
	flag.BoolVar(&SUBSET, "subset", false, "subset fasta")
	flag.BoolVar(&COUNT, "count", false, "count seq in fasta")
	flag.BoolVar(&TAGSEQ, "tagSeq", false, "when selecting Fasta seq, add tag to seqID using the second column of fasta_ID")
	flag.BoolVar(&TAGEXTRACT, "tagExtract", false, "when selecting Fasta seq, create a new fa file for each tag and the corresponding FASTA sequences  using the second column of fasta_ID")
	flag.BoolVar(&CLEAN, "clean", false, "clean fasta")
	flag.BoolVar(&SELECT, "select", false, "select a subset of  fasta seq")
	flag.BoolVar(&SIZE, "size", false, "get fasta seq size")
	flag.BoolVar(&SORT, "sort", false, "Sort fasta by size")
	flag.BoolVar(&TRIM, "trim", false, "trim name")
	flag.BoolVar(&UNIQUE, "unique", false, "rm dupplicates")
	flag.IntVar(&MAXPERCAT, "category_max", 10, "maximum of sequences per category")
	flag.IntVar(&NBTHREADS, "nbthreads", 5, "nb threads")
	flag.StringVar(&CATSEP, "category_sep", "_",
		"separator string to identify seq category in seq ID")
	flag.IntVar(&CATPOS, "category_pos", 1,
		"position of category string")
	flag.IntVar(&NBSPLIT, "n", 2, "Number of split")
	flag.Parse()

	switch {
	case SELECT:
		selectFasta()
	case SPLIT:
		splitFasta()
	case CLEAN:
		cleanFasta()
	case UNIQUE:
		uniqueFasta()
	case COUNT:
		countFasta()
	case SUBSET:
		subsetFasta()
	case SIZE:
		sizeFastaSeqs()
	case SORT:
		sortFastaSeqsBySize()
	case EXTRACT:
		extractFastaUsingPattern()

	default:
		fmt.Printf("#### NO OPTION PROVIDED ####\n\n")
		flag.Usage()
		return
	}
}

func extractFastaUsingPattern() {
	var writerMatching io.WriteCloser
	var writerNeq io.WriteCloser

	fasta := utils.FastaFile{}
	var eof bool
	var seqID, seq string

	bufferMatching := make([]bytes.Buffer, NBTHREADS)
	bufferNeq := make([]bytes.Buffer, NBTHREADS)
	var buffer *bytes.Buffer

	var i, j int

	guard := make(chan int, len(bufferMatching))

	for i = range bufferMatching {
		guard <- i
	}

	var doMatch, writeMatch, writeNeq bool

	if OUTFILE != "" {
		writerMatching = fileutils.ReturnWriter(OUTFILE)
		defer fileutils.CloseFile(writerMatching)
		writeMatch = true
		defer fmt.Printf("#### file written: %s\n", OUTFILE)
	}

	if OUTFILE2 != "" {
		writerNeq = fileutils.ReturnWriter(OUTFILE2)
		defer fileutils.CloseFile(writerNeq)
		writeNeq = true
		defer fmt.Printf("#### file written: %s\n", OUTFILE2)
	}

	i = <-guard

	for _, fastaf := range INPUTFAA {
		fasta.Init(fastaf)

		for !eof {
			eof, seqID, seq, _ = fasta.GetSeqIDSeqSizeSeqAndMovetoNext(TRIM)

			doMatch = strings.Index(seq, PATTERN) >= 0

			switch doMatch{
				case true && writeMatch:
				buffer = &bufferMatching[i]
				case false && writeNeq:
				buffer = &bufferNeq[i]
				default:
				buffer = nil
			}

			if buffer != nil {
				buffer.WriteRune('>')
				buffer.Write([]byte(seqID))
				buffer.WriteRune('\n')
				buffer.WriteString(seq)
				buffer.WriteRune('\n')
			}

			j++

			if j > 100 {
				writeBuffers(
					[]*bytes.Buffer{&bufferMatching[i], &bufferNeq[i]},
					[]*io.WriteCloser{&writerMatching, &writerNeq},
					i, guard)
				j = 0

				i = <-guard
			}
		}
	}

	writeBuffers(
		[]*bytes.Buffer{&bufferMatching[i], &bufferNeq[i]},
		[]*io.WriteCloser{&writerMatching, &writerNeq},
		i, guard)

	WAITING.Wait()
}


func writeBuffers(buffers []*bytes.Buffer, writers []*io.WriteCloser, chanID int, guard chan int) {
	WAITING.Add(1)
	defer WAITING.Done()
	MUTEX.Lock()

	for i, buffer := range buffers {
		if (*writers[i]) != nil {
			_, err := (*writers[i]).Write(buffer.Bytes())
			fileutils.Check(err)
		}

		buffer.Reset()
	}

	MUTEX.Unlock()
	guard <- chanID
}


func selectFasta() {
	if OUTFILE == "" {
		ext := path.Ext(INPUTFAA[0])
		OUTFILE = fmt.Sprintf("%s.selected%s", INPUTFAA[0][:len(INPUTFAA[0]) - len(ext)], ext)
	}

	SELECTEDSEQ = make(map[string]bool)
	SELECTEDSEQWITHTAG = make(map[string]string)

	fasta := utils.FastaFile{}

	reader, file := SEQSUBSETFILENAME.ReturnReader(0)
	defer fileutils.CloseFile(file)

	var split []string
	var seqID, tagID string

	if TAGEXTRACT {
		TAGSEQ = true
	}

	for reader.Scan() {
		line := reader.Text()

		split = strings.Split(line, "\t")
		seqID = split[0]

		if TAGSEQ {
			if len(split) < 2 {
				panic(fmt.Sprintf("Cannot extract sequence tag (2nd column) for line:%s (file: %s)\n",
					line, SEQSUBSETFILENAME.String()))
			}

			tagID = strings.Join(split[1:], "__")
		}

		split = strings.Split(seqID, "|")
		seqID = split[len(split) - 1]
		seqID = strings.Split(seqID, "__")[0]

		SELECTEDSEQ[seqID] = true

		if TAGSEQ {
			SELECTEDSEQWITHTAG[seqID] = tagID
		}
	}

	for _, fastaFile := range INPUTFAA {
		fasta.Init(fastaFile)

		switch {
		case TAGEXTRACT:
			splitFastaPerTag(fasta)
			return
		case TAGSEQ:
			fasta.WriteMatchingSeqIDWithMatchToBuff(SELECTEDSEQWITHTAG, false, true)
		default:
			fasta.WriteMatchingSeqIDToBuff(SELECTEDSEQ, false, true)
		}

		writer := fileutils.ReturnWriter(OUTFILE)
		defer fileutils.CloseFile(writer)

		_, err := writer.Write(fasta.WriteBuff())
		fileutils.Check(err)
	}

	fmt.Printf("file created with %s \n Number of sequences: %d\n",
		OUTFILE, len(SELECTEDSEQ))
}


func splitFastaPerTag(fasta utils.FastaFile) {

	var eof, isPresent bool
	var buffer * bytes.Buffer
	var seqID, seq, key string
	var err error

	writers := make(map[string]string)
	buffers := make(map[string]*bytes.Buffer)
	count := make(map[string]int)

	if _, err := os.Stat(OUTFILE); os.IsNotExist(err) {
		err = os.Mkdir(OUTFILE, os.ModePerm)
		fileutils.Check(err)
	}

	for _, key = range SELECTEDSEQWITHTAG {
		if _, isPresent = writers[key];!isPresent {
			outfile := fmt.Sprintf("%s/%s.fa", OUTFILE, key)
			writers[key] = outfile
			buffers[key] = &bytes.Buffer{}
		}
	}

	for !eof {
		eof, seqID, seq, _ = fasta.GetSeqIDSeqSizeSeqAndMovetoNext(TRIM)

		if key, isPresent = SELECTEDSEQWITHTAG[seqID];isPresent {
			buffer = buffers[key]
			buffer.WriteRune('>')
			buffer.WriteString(seqID)
			buffer.WriteRune('\n')
			buffer.WriteString(seq)
			buffer.WriteRune('\n')
			count[key]++
		}
	}

	for key, buffer = range buffers {
		writer := fileutils.ReturnWriter(writers[key])
		_, err = writer.Write(buffer.Bytes())
		fileutils.Check(err)
		buffer.Reset()
		fileutils.CloseFile(writer)
		outfile := fmt.Sprintf("%s/%s.fa", OUTFILE, key)
		fmt.Printf("## tag file written: %s (%d sequences)\n", outfile, count[key])

		if count[key] == 0 {
			os.Remove(outfile)
		}
	}
}

func sizeFastaSeqs() {
	fasta := utils.FastaFile{}

	var writer io.WriteCloser
	var eof, write bool
	var seqID string
	var size int

	if OUTFILE != "" {
		writer = fileutils.ReturnWriter(OUTFILE)
		defer fileutils.CloseFile(writer)
		write = true
	}

	for _, fastaf := range INPUTFAA {
		fasta.Init(fastaf)

		for !eof {
			eof, seqID, size = fasta.GetSeqIDSeqSizeAndMovetoNext()

			if write {
				_, err := writer.Write([]byte(fmt.Sprintf("%s\t%d\n", seqID, size)))
				fileutils.Check(err)
			} else {
				fmt.Printf("%s\t%d\n", seqID, size)
			}
		}
	}

	if write {
		fmt.Printf("file written: %s\n", OUTFILE)
	}
}


func sortFastaSeqsBySize() {
	fasta := utils.FastaFile{}

	var writer io.WriteCloser
	var eof bool
	var seqID, seq string
	var size int
	seqDict := make(map[string]string)

	type tuple struct {
		key string
		size int
	}
	var t1 tuple
	tuples := []tuple{}

	if OUTFILE == "" {
		panic("Error -out must be defined!")
	}

	writer = fileutils.ReturnWriter(OUTFILE)
	defer fileutils.CloseFile(writer)

	for _, fastaf := range INPUTFAA {
		fasta.Init(fastaf)

		for !eof {
			eof, seqID, seq, size = fasta.GetSeqIDSeqSizeSeqAndMovetoNext(TRIM)
			t1.key = seqID
			t1.size = size
			seqDict[seqID] = seq

			tuples = append(tuples, t1)
		}
	}

	sort.Slice(tuples, func(i, j int) bool {
		return tuples[i].size > tuples[j].size
	})

	buffer := bytes.Buffer{}
	i := 0

	for _, t1 = range tuples {
		buffer.WriteRune('>')
		buffer.Write([]byte(t1.key))
		buffer.WriteString(" size:")
		buffer.WriteString(strconv.Itoa(t1.size))
		buffer.WriteRune('\n')
		buffer.WriteString(seqDict[t1.key])
		buffer.WriteRune('\n')

		i++

		if i > 100 {
			writer.Write(buffer.Bytes())
			buffer.Reset()
			i = 0
		}
	}

	writer.Write(buffer.Bytes())
	buffer.Reset()

	fmt.Printf("file written: %s\n", OUTFILE)
}

func subsetFasta() {
	fasta := utils.FastaFile{}

	if OUTFILE == "" {
		ext := path.Ext(INPUTFAA[0])
		OUTFILE = fmt.Sprintf("%s.subset%s", INPUTFAA[0][:len(INPUTFAA[0]) - len(ext)], ext)
	}

	writer := fileutils.ReturnWriter(OUTFILE)
	defer fileutils.CloseFile(writer)

	catmap := make(map[string][]string)
	tStart := time.Now()

	var eof bool
	var seqID, category string
	var split []string
	count := 0

	fasta.Init(INPUTFAA[0])

	for !eof {
		eof, seqID = fasta.GetSeqIDAndMovetoNext(true)
		split = strings.Split(seqID, CATSEP)

		if len(split) <= CATPOS {
			panic(
				fmt.Sprintf("sequence ID: %s cannot be splitted in %d with %s",
					seqID, CATPOS, CATSEP))
		}

		category = split[CATPOS]
		catmap[category] = append(catmap[category], seqID)
		count++
	}

	tDiff := time.Since(tStart)

	catExample := []string{}
	count2 := 0

	for category = range catmap {
		catExample = append(catExample, category)
		count2++

		if count2 > 3 {
			break
		}
	}

	fmt.Printf("1rst Screening done in time: %f s \n", tDiff.Seconds())
	fmt.Printf("Number of caterogies (i.e. %s): %d for a total of %d seq\n",
		catExample, len(catmap), count)

	chunk := len(catmap) / NBTHREADS

	WAITING = sync.WaitGroup{}
	MUTEX = sync.Mutex{}
	SELECTEDSEQ = make(map[string]bool)
	catlist := []string{}
	count = 0

	tStart = time.Now()
	rand.Seed(time.Now().Unix())

	for cat := range catmap {
		catlist = append(catlist, cat)
		count++

		if count >= chunk {
			go randomCatList(&catmap, catlist)
			count = 0
			catlist = []string{}
		}
	}

	go randomCatList(&catmap, catlist)

	WAITING.Wait()
	tDiff = time.Since(tStart)
	fmt.Printf(" Randomization done in time: %f s \n", tDiff.Seconds())

	fasta.Init(INPUTFAA[0])
	fasta.WriteMatchingSeqIDToBuff(SELECTEDSEQ, false, false)
	_, err := writer.Write(fasta.WriteBuff())

	fileutils.Check(err)

	fmt.Printf("file created with %s \n Number of sequences: %d\n",
		OUTFILE, len(SELECTEDSEQ))
}


func randomCatList(catmap * map[string][]string, catlist []string) {
	WAITING.Add(1)
	defer WAITING.Done()

	seqIds := []string{}
	var index int

	for _, cat := range catlist {
		for i := 0;i < MAXPERCAT;i++ {
			index = rand.Intn(len((*catmap)[cat]))
			seqIds = append(seqIds, (*catmap)[cat][index])
		}
	}

	MUTEX.Lock()
	for _, seq := range seqIds {
		SELECTEDSEQ[seq] = true
	}

	MUTEX.Unlock()
}


func cleanFasta() {
	fasta := utils.FastaFile{}

	if OUTFILE == "" {
		ext := path.Ext(INPUTFAA[0])
		OUTFILE = fmt.Sprintf("%s.clean%s", INPUTFAA[0][:len(INPUTFAA[0]) - len(ext)], ext)
	}

	tStart := time.Now()

	writer := fileutils.ReturnWriter(OUTFILE)
	defer fileutils.CloseFile(writer)
	var err error

	for _, inputfaa := range INPUTFAA {

		fasta.Init(inputfaa)
		var count, count2, count3 int
		var eof, clean bool

		for !eof {
			eof, clean = fasta.SaveCurrentSeqIfCleantoBuffAndMove(true)

			if clean {
				count2++
			} else {
				count3++
			}

			count++
		}

		if count2 > 50000 {
			_, err = writer.Write(fasta.WriteBuff())
			fileutils.Check(err)
			count2 = 0
		}

		_, err = writer.Write(fasta.WriteBuff())
		fileutils.Check(err)
		fmt.Printf("Fil written: %s\n", OUTFILE)
		tDiff := time.Since(tStart)
		fmt.Printf("done in time: %f s \n", tDiff.Seconds())

	}
}


func uniqueFasta() {
	fasta := utils.FastaFile{}

	if OUTFILE == "" {
		ext := path.Ext(INPUTFAA[0])
		OUTFILE = fmt.Sprintf("%s.unique%s", INPUTFAA[0][:len(INPUTFAA[0]) - len(ext)], ext)
	}

	tStart := time.Now()

	writer := fileutils.ReturnWriter(OUTFILE)
	defer fileutils.CloseFile(writer)
	var err error
	var seqID string
	var count, count2, count3 int

	SELECTEDSEQ = make(map[string]bool)

	for _, inputfaa := range INPUTFAA {

		fasta.Init(inputfaa)
		var eof bool

		for !eof {
			seqID = fasta.GetSeqID()

			if TRIM {
				seqID = strings.Split(seqID, CATSEP)[CATPOS]
				fasta.SetSeqID(seqID)
			}

			if !SELECTEDSEQ[seqID] {
				eof = fasta.SaveCurrentSeqToBuffAndMove(false, "")
				count2++
				count++
				SELECTEDSEQ[seqID] = true
			} else {
				eof = fasta.MovetoNextSeqID()
				count3++
			}
		}

		if count2 > 50000 {
			_, err = writer.Write(fasta.WriteBuff())
			fileutils.Check(err)
			count2 = 0
		}

		_, err = writer.Write(fasta.WriteBuff())
		fileutils.Check(err)

	}

	fmt.Printf("File written: %s\n", OUTFILE)
	fmt.Printf("Number of sequences written: %d\n Number of dupplicates: %d\n",
		count, count3)
	tDiff := time.Since(tStart)
	fmt.Printf("done in time: %f s \n", tDiff.Seconds())
}


func countFasta() {
	fasta := utils.FastaFile{}
	tStart := time.Now()

	for _, inputfaa := range INPUTFAA {
		fasta.Init(inputfaa)
		nbSeq := fasta.ScanForNbSeq()
		fmt.Printf("Number of sequences: %d\n", nbSeq)

	}

	tDiff := time.Since(tStart)
	fmt.Printf("done in time: %f s \n", tDiff.Seconds())
}


func splitFasta() {
	fasta := utils.FastaFile{}
	fasta.Init(INPUTFAA[0])

	tStart := time.Now()
	nbSeq := fasta.ScanForNbSeq()
	fmt.Printf("Number of sequences to split: %d\n", nbSeq)
	var err error

	filenb := 0
	chunk := nbSeq / NBSPLIT
	ext := path.Ext(INPUTFAA[0])

	if OUTPATH != "" {
		OUTPATH = fmt.Sprintf("%s/", OUTPATH)
	}

	matches, _ := filepath.Glob(fmt.Sprintf("%s%s_%d%s",
		OUTPATH, PREFIX, filenb, ext))

	for _, match := range matches {
		err = os.Remove(match)
		fileutils.Check(err)
	}

	output := fmt.Sprintf("%s%s_%d%s",
		OUTPATH, PREFIX, filenb, ext)
	writer := fileutils.ReturnWriter(output)
	count, count2 := 0, 0
	eof := false

	for !eof {
		if count > chunk {
			_, err = writer.Write(fasta.WriteBuff())
			fileutils.Check(err)
			err = writer.Close()
			fileutils.Check(err)
			fmt.Printf("Fil written: %s\n", output)
			filenb++
			output = fmt.Sprintf("%s%s_%d%s",
				OUTPATH, PREFIX, filenb, ext)
			writer = fileutils.ReturnWriter(output)
			count = 0
		}

		eof = fasta.SaveCurrentSeqToBuffAndMove(false, "")

		count++
		count2++

		if count2 > 100000 {
			_, err = writer.Write(fasta.WriteBuff())
			fileutils.Check(err)
			count2 = 0
		}
	}

	_, err = writer.Write(fasta.WriteBuff())
	fileutils.Check(err)
	err = writer.Close()
	fileutils.Check(err)
	fmt.Printf("Fil written: %s\n", output)
	tDiff := time.Since(tStart)
	fmt.Printf("done in time: %f s \n", tDiff.Seconds())
}
