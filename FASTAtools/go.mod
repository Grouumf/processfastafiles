module gitlab.com/Grouumf/processfastafiles/FASTAtools

go 1.15

require (
	gitlab.com/Grouumf/ATACdemultiplex v0.48.2
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20210802220835-4b20ff939e6b
	gitlab.com/Grouumf/processfastafiles/fastaUtils v0.0.0-20220112172845-b345a4d3dd9d
)
