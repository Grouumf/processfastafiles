package main


import (
	utils "gitlab.com/Grouumf/processfastafiles/fastaUtils"
	"flag"
	"fmt"
	"os"
	"sync"
	"time"
	"io"
	"path"
	fileutils "gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils"
)



/*BLASTRESULTFOLDERS result for blast file in format 7*/
var BLASTRESULTFOLDERS utils.FolderListFlags

/*REGEXFOLDER regex to capture specific genomes */
var REGEXFOLDER string

/*OUTFILE out */
var OUTFILE string

/*NBTHREADS nb threads */
var NBTHREADS int

/*MUTEX ...*/
var MUTEX sync.Mutex

/*TOPHITCUTOFF top hit cutoff */
var TOPHITCUTOFF int

/*BLASTSCORECUTOFF blast score cutoff */
var BLASTSCORECUTOFF float64

/*NBHIT total number of hit */
var NBHIT int

/*NBQUERIES total number of hit */
var NBQUERIES int

/*DOGRAPH do graph*/
var DOGRAPH bool

/*QUERYSET ...*/
var QUERYSET map[string]bool


func main() {
	flag.Usage = func() {
		fmt.Fprintf(
			os.Stderr, `
################ Module to process specifically blast output  ##############


USAGE:
BLASTtools -create_graph -in <blast folder> -out <file>
 (optional -regex <str> -threads <int> -in <blast folder2> ...)
############################################################################

`)
		flag.PrintDefaults()
	}

	flag.Var(&BLASTRESULTFOLDERS, "in",
		"name of the folder having blast type results. each results should be in format 7")

	flag.IntVar(&NBTHREADS, "threads", 4,
		"nb treads to use")

	flag.IntVar(&TOPHITCUTOFF, "top_cutoff", 30,
		"Top hit cutoff (after K top hits, move to next query)")

	flag.Float64Var(&BLASTSCORECUTOFF, "score", 0,
		"Blast -log10(evalue cutoff) ")

	flag.StringVar(&REGEXFOLDER, "regex", "",
		"optional regex to capture only specific folders")

	flag.StringVar(&OUTFILE, "out", "",
		"optional regex to capture only specific folders")

	flag.BoolVar(&DOGRAPH, "create_graph", false,
		"create graph input file")

	flag.Parse()

	switch {
	case DOGRAPH:
		createGraph()
	default:
		fmt.Printf("#### No option provided! #### \n\n")
		flag.Usage()
		os.Exit(1)
	}
}


func createGraph() {
	if OUTFILE == "" {
		panic("-out should not be empty")
	}

	MUTEX = sync.Mutex{}

	QUERYSET = make(map[string]bool)

	writer := fileutils.ReturnWriter(OUTFILE)
	defer fileutils.CloseFile(writer)

	fmt.Printf("scanning folders...\n")
	waiting := sync.WaitGroup{}
	waiting.Add(len(BLASTRESULTFOLDERS))

	for _, folder := range BLASTRESULTFOLDERS {
		go folder.ListFiles(REGEXFOLDER, &waiting)
	}

	tStart := time.Now()
	waiting.Wait()
	tDiff := time.Since(tStart)
	fmt.Printf("Scanning done in time: %f s \n", tDiff.Seconds())

	tStart = time.Now()

	for _, folder := range BLASTRESULTFOLDERS {
		guard := make(chan int, NBTHREADS)
		count := 0

		nbFiles := len(folder.GetFileNames())

		fmt.Printf("Processing %d files\n", nbFiles)

		for _, blastFile := range folder.GetFileNames() {
			guard <- count

			waiting.Add(1)
			blastFile = path.Join(folder.String(), blastFile)
			go createGraphOneThreads(blastFile, &writer, &waiting, guard)
			count++
			fmt.Printf("\rNb files processed: %d / %d ", count, nbFiles)
		}
	}

	waiting.Wait()

	tDiff = time.Since(tStart)
	fmt.Printf("Graph input file creation done in: %f s \n", tDiff.Seconds())
	fmt.Printf("file created: %s with %d queries and  %d links\n",
		OUTFILE, len(QUERYSET), NBHIT)
}


func createGraphOneThreads(blastFile string,
	writer *io.WriteCloser,
	waiting * sync.WaitGroup, guard chan int) {
	defer waiting.Done()
	var err error

	blast := utils.BlastResultFmt7{}
	blast.Init(blastFile)
	defer blast.Close()
	blast.WriteHitsToBuffers(TOPHITCUTOFF, BLASTSCORECUTOFF)

	MUTEX.Lock()
	_, err = (*writer).Write(blast.WriteBuff())
	fileutils.Check(err)
	NBHIT += blast.GetNbHits()
	NBQUERIES += blast.GetNbQueries()

	for _, query := range blast.QueriesList {
		QUERYSET[query] = true
	}
	MUTEX.Unlock()
	<-guard
}
