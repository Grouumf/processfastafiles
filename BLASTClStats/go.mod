module gitlab.com/Grouumf/processfastafiles/BLASTClStats

go 1.14

require (
	github.com/mroth/weightedrand v0.4.1 // indirect
	github.com/pkg/profile v1.5.0
	gitlab.com/Grouumf/ATACdemultiplex v0.48.2
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20210802220835-4b20ff939e6b
	gitlab.com/Grouumf/processfastafiles/fastaUtils v0.0.0-20221021224727-2e95bcd3ed45
)
