package main


import (
	utils "gitlab.com/Grouumf/processfastafiles/fastaUtils"
	"flag"
	"fmt"
	"path"
	fileutils "gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils"
	"os"
	"strings"
	"regexp"
	"sync"
	"time"
	"math"
	"bytes"
	"strconv"
)

/*HMMERRESULTFOLDER result for HMM domain for input sequences*/
var HMMERRESULTFOLDER utils.Foldername

/*BLASTRESULTREF Blast files results for reference sequences in format 7*/
var BLASTRESULTREF utils.Foldername

/*HMMERRESULTREF result for HMM domain for ref sequences*/
var HMMERRESULTREF utils.Foldername

/*OUTFILES base name for output files */
var OUTFILES string

/*FASTAREFFILE FASTA ref sequences*/
var FASTAREFFILE fileutils.Filename

/*MCLRESULTSFILE MCL clustering result file*/
var MCLRESULTSFILE fileutils.Filename

/*ALPHA threshold */
var ALPHA float64

/*WORKFLOW construct an index on HMM*/
var WORKFLOW bool

/*REGEXFOLDER regex to capture specific genomes */
var REGEXFOLDER string

/*NBTHREADS nb threads */
var NBTHREADS int

/*MAXOPENFILES max number of open files */
var MAXOPENFILES int

/*RANDOMNBIT number of random iterations for p-value computation */
var RANDOMNBIT int

/*CLASSWEIGHT mixing weight used to construct random vectors */
var CLASSWEIGHT float64

/*STRATEGY strategy to use to select clusters (v1 or v2) */
var STRATEGY string


type stats struct {
	pvalue, score float64
	nbSeqs int
	isValid bool
	ratio, homogeneity float64
	annotation string
}

const  (
	v1strategy string = "v1"
	v2strategy string = "v2"
)


func checkStrategy( str string) {
	switch {
	case str == v1strategy:
	case str == v2strategy:
	default:
		panic(fmt.Sprintf("Not valid Stratgy %s\n", str))
	}
}

//////////////// INSTANCES ////////////////////

/*SEQIDTOREFMATCH sequence ID from blast to reference seq best match */
var SEQIDTOREFMATCH map[string]string

/*HMMREFINSTANCE class for HMM res */
var HMMREFINSTANCE utils.HmmerResults

/*HMMSEQINSTANCE class for HMM res */
var HMMSEQINSTANCE utils.HmmerResults

/*CLUSTERREF instance of clustering for ref seq*/
var CLUSTERREF utils.MCLResults

/*CLUSTERSEQ instance of clustering for seq*/
var CLUSTERSEQ utils.MCLResults

/*CLUSTERSSTATS  cluster statistics mapping*/
var CLUSTERSSTATS map[int]stats

/*OPENFILESCHAN  channel for open file */
var OPENFILESCHAN chan bool
////////////////////////////////////////////////




func main() {
	flag.Usage = func() {
		fmt.Fprintf(
			os.Stderr, `
###### Module to create index files for clustering / hmmer resuits  #########
Statistical analysis wokrflow for blast clustering

USAGE:
# Compute statistics for each cluster using reference fasta, all-vs-all like blast results,
all-vs-ref blast results, ref motif results, all seq, motif results

BLASTClStats -workflow  \
             -blastResRef <blast folder> \
             -hmmRes <hmm folder > \
             -hmmResRef <hmm folder > \
             -MCLRes <MCL file > \
             -fastaRef <fasta file > \
             -alpha <float> \
             -out <string> \
             -weight <float> \
             -threads <int> (OPTIONAL) \
             -regex <string> (OPTIONAL) \
             -strategy <string> \
             -max_openFiles <int> (OPTIONAL)

############################################################################

`)
		flag.PrintDefaults()
	}

	flag.StringVar(&OUTFILES, "out", "",
		"base name for output files")

	flag.StringVar(&STRATEGY, "strategy", "v1",
		"clustering selection strategy (v1 or v2)")

	flag.StringVar(&REGEXFOLDER, "regex", ".*",
		"optional regex to capture only specific HMM folders (debugging)")

	flag.Var(&BLASTRESULTREF, "blastResRef",
		"Blast files results for reference sequences in format 7")

	flag.IntVar(&NBTHREADS, "threads", 4,
		"nb treads to use")

	flag.IntVar(&RANDOMNBIT, "nb_it", 200,
		"number of random iterations for p-value computation")

	flag.IntVar(&MAXOPENFILES, "max_openFiles", 40,
		"Maximum number of open files")

	flag.Var(&HMMERRESULTFOLDER, "hmmRes",
		"name of the folder having blast type results. each results should be in format 7")

	flag.Var(&HMMERRESULTREF, "hmmResRef", "result for HMM domain for ref sequences")

	flag.Var(&FASTAREFFILE, "fastaRef", "result for HMM domain for ref sequences")

	flag.Var(&MCLRESULTSFILE, "MCLRes", "MCL clustering result file")

	flag.Float64Var(&ALPHA, "alpha", 0.10, "Statistical threshold (p-value cutoff)")
	flag.Float64Var(&CLASSWEIGHT, "weight", 0.50, "mixing weight used to construct random vectors")

	flag.BoolVar(&WORKFLOW, "workflow", false, "Launch statistical cl workflow to score cluster")

	flag.Parse()

	if BLASTRESULTREF.String() == "" {
		panic("-blastResRef should be defined")
	}

	if HMMERRESULTFOLDER.String() == "" {
		panic("-hmmRes should be defined")
	}

	if HMMERRESULTREF.String() == "" {
		panic("-hmmResRef should be defined")
	}

	if FASTAREFFILE.String() == "" {
		panic("-fastaRef should be defined")
	}

	if MCLRESULTSFILE.String() == "" {
		panic("-fastaRef should be defined")
	}

	if OUTFILES == "" {
		ext := path.Ext(MCLRESULTSFILE.String())

		OUTFILES = fmt.Sprintf("%s.out",
			MCLRESULTSFILE[:len(MCLRESULTSFILE) - len(ext)])
	}

	OPENFILESCHAN = make(chan bool, MAXOPENFILES)

	for i := 0;i < MAXOPENFILES;i ++ {
		OPENFILESCHAN <- true
	}

	checkStrategy(STRATEGY)

	switch {
	case WORKFLOW:
		doWorkflow()
	default:
		fmt.Printf("#### No option provided ####\n")
		flag.Usage()
	}

}


func doWorkflow() {
	HMMREFINSTANCE = utils.HmmerResults{}
	HMMSEQINSTANCE = utils.HmmerResults{}

	<-OPENFILESCHAN
	<-OPENFILESCHAN

	HMMREFINSTANCE.Init("", true)
	HMMSEQINSTANCE.Init("", true)

	waiting := sync.WaitGroup{}

	waiting.Add(2)

	go createClusterRefInstance(&waiting)
	go createClusterSeqInstance(&waiting)

	waiting.Wait()
	fmt.Printf("Reference an query cluster instances created!\n")
	fmt.Printf("#### NUMBER OF REFERENCE SEQUENCES WITH DOMAINS: %d\n",
		len(HMMREFINSTANCE.SeqIDToDomains))

	CLUSTERREF.CreateDomainClusterVectors(HMMREFINSTANCE.SeqIDToDomains)

	doClusterStatisticalAnalysis()

	writeClMatrix()
	writeOutputs()
}

func writeClMatrix() {

	var genomeID, seq string
	var isInside bool
	var split []string
	var matrixCl map[string]map[int]int
	var matrixAnnotation map[string]map[string]int

	matrixCl = make(map[string]map[int]int)
	matrixAnnotation = make(map[string]map[string]int)

	for cl, stat := range CLUSTERSSTATS {
		seqs := CLUSTERSEQ.GetAllSeqIDsfromClID(cl)

		for _, seq = range seqs {
			if _, isInside = SEQIDTOREFMATCH[seq];!isInside {
				continue
			}

			split = strings.SplitAfter(seq, "_")
			genomeID = strings.Trim(split[0] + split[1], "_")

			if _, isInside = matrixCl[genomeID];!isInside {
				matrixCl[genomeID] = make(map[int]int)
				matrixAnnotation[genomeID] = make(map[string]int)
			}

			matrixCl[genomeID][cl]++
			matrixAnnotation[genomeID][stat.annotation]++
		}
	}

	writeClMatrixFile(matrixCl)
	writeAnnMatrixFile(matrixAnnotation, "clAnnotation")
}

func writeClMatrixFile(matrix map[string]map[int]int) {
	matFout := fmt.Sprintf("%s.allCLusters.coo.mat.gz", OUTFILES)

	writer := fileutils.ReturnWriter(matFout)
	defer fileutils.CloseFile(writer)

	buffer := bytes.Buffer{}

	var err error

	for genomeID, vect := range matrix {

		for cl, value := range vect {
			buffer.WriteString(genomeID)
			buffer.WriteRune('\t')
			buffer.WriteString(strconv.Itoa(cl))
			buffer.WriteRune('\t')
			buffer.WriteString(strconv.Itoa(value))
			buffer.WriteRune('\n')
		}


		_,err = writer.Write(buffer.Bytes())
		fileutils.Check(err)
		buffer.Reset()
	}

	_, err = writer.Write(buffer.Bytes())
	fileutils.Check(err)
	buffer.Reset()

	fmt.Printf("#### Matrix file written: %s\n", matFout)
}

func writeAnnMatrixFile(matrix map[string]map[string]int, key string) {
	matFout := fmt.Sprintf("%s.%s.coo.mat.gz", OUTFILES, key)

	writer := fileutils.ReturnWriter(matFout)
	defer fileutils.CloseFile(writer)

	buffer := bytes.Buffer{}
	var err error

	for genomeID, vect := range matrix {

		for ann, value := range vect {
			buffer.WriteString(genomeID)
			buffer.WriteRune('\t')
			buffer.WriteString(ann)
			buffer.WriteRune('\t')
			buffer.WriteString(strconv.Itoa(value))
			buffer.WriteRune('\n')
		}

		_,err = writer.Write(buffer.Bytes())
		fileutils.Check(err)
		buffer.Reset()
	}

	_,err = writer.Write(buffer.Bytes())
	fileutils.Check(err)
	buffer.Reset()

	fmt.Printf("#### Matrix file written: %s\n", matFout)
}

func writeOutputs() {
	validFout := fmt.Sprintf("%s.tsv", OUTFILES)

	writer := fileutils.ReturnWriter(validFout)
	defer fileutils.CloseFile(writer)

	nbSeqsFail := 0
	nbSeqsGood := 0
	goodClIDs := []int{}

	buffer := bytes.Buffer{}
	var err error

	buffer.WriteString("clID\tannotation\tscore\tpval\tnb_sequences\tisValid\tHratio\tHomoegenity\n")

	for cl, stat := range CLUSTERSSTATS {
		buffer.WriteString(fmt.Sprintf("%d\t%f\t%s\t%f\t%d\t%t\t%f\t%f\n",
			cl,
			stat.score,
			stat.annotation,
			stat.pvalue,
			stat.nbSeqs,
			stat.isValid,
			stat.ratio,
			stat.homogeneity))

		if !stat.isValid {
			nbSeqsFail += stat.nbSeqs
		} else {
			goodClIDs = append(goodClIDs, cl)
			nbSeqsGood += stat.nbSeqs
		}
	}

	_, err = writer.Write(buffer.Bytes())
	fileutils.Check(err)
	buffer.Reset()
	fmt.Printf("File created: %s\n", validFout)

	var isInside bool

	allGoodSeq := make(map[string]bool)
	allGoodSeqClID := make(map[string]int)

	for _, clID := range goodClIDs {
		for _, seq := range CLUSTERSEQ.GetAllSeqIDsfromClID(clID) {

			if _, isInside = SEQIDTOREFMATCH[seq];!isInside {
				continue
			}

			allGoodSeq[seq] = true
			allGoodSeqClID[seq] = clID
		}
	}

	writeAnnMatrixFile(HMMSEQINSTANCE.ReturnDomainMatrix(allGoodSeq), "domain")
	HMMSEQINSTANCE.WriteSeqDomainToBuffer(allGoodSeq)

	fmt.Printf("#### Number of filtered sequences: %d\n", nbSeqsFail)
	fmt.Printf("#### Number of kept sequences frm clustering: %d\n", nbSeqsGood)
	fmt.Printf("#### Number of query sequences matching ref sequences: %d\n", len(SEQIDTOREFMATCH))

	domainFout := fmt.Sprintf("%s.domain.txt", OUTFILES)
	writer2 := fileutils.ReturnWriter(domainFout)
	defer fileutils.CloseFile(writer2)

	_, err = writer2.Write(HMMSEQINSTANCE.WriteBuff())
	fileutils.Check(err)

	fmt.Printf("File written: %s\n", domainFout)

	writeValidCluster(allGoodSeqClID)
}


func writeValidCluster(allGoodSeqClID map[string]int) {
	var err error
	clFout := fmt.Sprintf("%s.valid_cluster.txt", OUTFILES)
	writer3 := fileutils.ReturnWriter(clFout)
	defer fileutils.CloseFile(writer3)

	for seq, clID := range allGoodSeqClID {
		writer3.Write(
			[]byte(fmt.Sprintf("%s\t%s\t%d\t%t\n",
				seq, CLUSTERSSTATS[clID].annotation, clID,
				CLUSTERSSTATS[clID].isValid)))

	}

	_, err = writer3.Write(CLUSTERSEQ.WriteBuff())
	fileutils.Check(err)

	fmt.Printf("File written: %s\n", clFout)
}

func doClusterStatisticalAnalysis() {

	var cluster, size int

	CLUSTERSSTATS = make(map[int]stats)

	mutex := sync.Mutex{}
	waiting := sync.WaitGroup{}
	nbClusters := len(CLUSTERSEQ.ClusterDomainsVector)

	tStart := time.Now()

	guard := make(chan bool, NBTHREADS)

	for i := 0 ;i < NBTHREADS;i++ {
		guard <- true
	}

	fmt.Printf("Computing cluster statistics for %d clusters...\n", nbClusters)

	count := 0

	for cluster, size = range CLUSTERSEQ.GetClSizeMap() {
		waiting.Add(1)
		<- guard
		computeOneClusterStat(
			cluster,
			size,
			guard,
			&mutex,
			&waiting)

		fmt.Printf("\r clusters ( %d / %d ) ",
			count, nbClusters )
		count++
	}

	waiting.Wait()

	tDiff := time.Since(tStart)
	fmt.Printf("\nCluster statistics done in: %f s \n", tDiff.Seconds())

}

func computeOneClusterStat(cluster, size int,
	guard chan bool,
	mutex * sync.Mutex,
	waiting * sync.WaitGroup) {

	var seqIDs, refSeqIDs []string
	var randomVectorList []map[string]float64
	var refVector, seqVector map[string]float64
	var matchingCluster int
	var seqID, refSeqID string
	var isInside bool
	stati := stats{}
	var matchingClusterStr string

	var score, pvalue, ratio, halfEntropy float64

	defer waiting.Done()

	seqIDs = CLUSTERSEQ.GetAllSeqIDsfromClID(cluster)

	count := 0

	for _, seqID = range seqIDs {
		if refSeqID, isInside = SEQIDTOREFMATCH[seqID];!isInside {
			count++
			continue

		}
		refSeqIDs = append(refSeqIDs, refSeqID)
	}

	if len(refSeqIDs) == 0 {
		fmt.Printf("\nCluster ID %d ([%s ...]) empty. Not computing statistics...\n",
			cluster, seqIDs[0])
		guard <- true
		return
	}

	matchingCluster, matchingClusterStr, ratio, halfEntropy = CLUSTERREF.GetMostRepresentedCluster(
		refSeqIDs, true)

	refVector = CLUSTERREF.ClusterDomainsVector[matchingCluster]
	seqVector = CLUSTERSEQ.ClusterDomainsVector[cluster]

	if len(refVector) == 0 {
		fmt.Printf("Error! Domain Matching cluster for ref clustering %d (%s) [%s ...] is empty",
			matchingCluster, matchingClusterStr, refSeqIDs[0])
	}

	if len(seqVector) == 0 {
		fmt.Printf("seqVector empty for clID %d (seq ID: [%s ... ])\n", cluster, seqIDs[0])

	}

	switch STRATEGY {
	case "v1":
		// Old strategy
		randomVectorList = CLUSTERSEQ.ReturnDomainVectorsForRandomClusters(size, RANDOMNBIT)
	case "v2":
	// New stratey
		randomVectorList = CLUSTERSEQ.ReturnDomainVectorsForRandomClustersV2(
			cluster, size, RANDOMNBIT, CLASSWEIGHT)
	default:
		panic(fmt.Sprintf("Not valid strategy: %s\n", STRATEGY))
	}

	score, pvalue = computeDistancesandPvalue(refVector, seqVector, randomVectorList)

	stati.pvalue = pvalue
	stati.score = score
	stati.isValid = pvalue < ALPHA && size > 3
	stati.nbSeqs = size
	stati.ratio = ratio
	stati.annotation = matchingClusterStr
	stati.homogeneity = halfEntropy / float64(len(SEQIDTOREFMATCH))

	mutex.Lock()
	CLUSTERSSTATS[cluster] = stati
	mutex.Unlock()

	guard <- true

}


func computeDistancesandPvalue(refVector, seqVector map[string]float64,
	randomVectorList []map[string]float64) (score, pvalue float64) {

	score = computeCosineDistance(refVector, seqVector)
	var randScore float64

	for _, randVector := range randomVectorList {
		randScore = computeCosineDistance(refVector, randVector)

		if score >= randScore {
			pvalue++
		}
	}

	pvalue = pvalue / float64(len(randomVectorList))

	return score, pvalue
}

func computeCosineDistance( vect1, vect2 map[string]float64) (cosine float64) {

	var denom, numer1, numer2, score1, score2 float64
	var isInside bool
	var key1, key2 string

	for key1, score1 = range vect1 {
		if score2, isInside = vect2[key1];isInside {
			denom += score1 * score2

			numer2 += score2 * score2
		}

		numer1 += score1 * score1

	}

	for key2, score2 = range vect2 {
		if _, isInside = vect1[key2];!isInside {

			numer2 += score2 * score2
		}
	}


	cosine = 1 - (1 + denom) / (math.Sqrt(1 + numer1) * math.Sqrt(1 + numer2))

	return cosine
}


func createClusterSeqInstance(waiting * sync.WaitGroup) {

	defer waiting.Done()

	waitingSeqID := sync.WaitGroup{}

	createSeqIDToRefMatch()

	seqIDs := make(map[string]bool)

	for seqID := range SEQIDTOREFMATCH {
		seqIDs[seqID] = true
	}

	waitingSeqID.Add(1)

	tStart := time.Now()
	fmt.Printf(
		"Scanning HMM result files and instanciating clusters for query sequences...\n")

	regex := fmt.Sprintf("%s.*.txt", REGEXFOLDER)

	go scanHMMFolder(
		&waitingSeqID,
		HMMERRESULTFOLDER,
		&HMMSEQINSTANCE,
		seqIDs,
		regex,
		"HMMER genome FILES")

	<-OPENFILESCHAN

	CLUSTERSEQ.Init(MCLRESULTSFILE.String())
	fmt.Printf(
		"Scanning cluster results file...\n")
	nbCl := CLUSTERSEQ.ConstrutSeqIDtoClIDMapping()
	fmt.Printf(
		"Cluster results file scanned! (%d clusters)\n", nbCl)

	waitingSeqID.Wait()

	fmt.Printf("#### NUMBER OF QUERY SEQUENCES WITH DOMAINS: %d\n",
		len(HMMSEQINSTANCE.SeqIDToDomains))

	fmt.Printf("Creating domain vectors...!\n")
	CLUSTERSEQ.CreateDomainClusterVectors(HMMSEQINSTANCE.SeqIDToDomains)
	tDiff := time.Since(tStart)
	fmt.Printf("Domain clusters for query sequences done in time: %f s \n", tDiff.Seconds())
}


func createSeqIDToRefMatch() {
	waiting := sync.WaitGroup{}

	waiting.Add(1)
	BLASTRESULTREF.ListFiles(REGEXFOLDER, &waiting)
	fmt.Printf("Finding best reference seq matches for query sequences... \n")
	SEQIDTOREFMATCH = make(map[string]string)
	mutex := sync.Mutex{}

	guard := make(chan bool, NBTHREADS)

	for i := 0 ;i < NBTHREADS;i++ {
		guard <- true
	}
	tStart := time.Now()
	nbFiles := BLASTRESULTREF.GetNbFiles()

	fmt.Printf("#### number of BLAST files scanned: %d\n", nbFiles)

	for index, file := range BLASTRESULTREF.GetFileNames() {
		matchingFile, isFound := BLASTRESULTREF.GetFilePath(file)

		if !isFound {
			panic(fmt.Sprintf("File not found: %s\n", file))
		}

		<- guard
		waiting.Add(1)
		go updateSeqIDToRefMatch(
			matchingFile,
			&waiting,
			&mutex,
			guard)

		fmt.Printf("\r BLAST processed files (%d / %d )   ",
			index, nbFiles)
	}

	fmt.Printf("\n")

	waiting.Wait()
	tDiff := time.Since(tStart)
	fmt.Printf("Best matches for query sequences done in time: %f s \n", tDiff.Seconds())
	fmt.Printf("#### Number of query sequences matching ref sequences: %d\n", len(SEQIDTOREFMATCH))
}

func updateSeqIDToRefMatch(
	file string,
	waiting * sync.WaitGroup,
	mutex * sync.Mutex,
 	guard chan bool) {

	defer waiting.Done()

	blast := utils.BlastResultFmt7{}
	<-OPENFILESCHAN
	blast.Init(file)

	matches := blast.GetAllSeqIDsWithMatch()

	mutex.Lock()
	defer mutex.Unlock()

	for seqID, match := range matches {
		SEQIDTOREFMATCH[seqID] = match
	}

	blast.Close()
	OPENFILESCHAN <- true

	guard <- true
}

func createClusterRefInstance(waiting * sync.WaitGroup) {
	defer waiting.Done()

	fasta := utils.FastaFile{}

	<-OPENFILESCHAN
	fasta.Init(FASTAREFFILE.String())

	tStart := time.Now()
	fmt.Printf("Scanning HMM files and instanciating clusters for reference sequences ....\n")
	fmt.Printf("Scanning reference FASTA file...\n")
	seqIDs := fasta.GetAllSeqIDs(false)
	seqIDsTrimmed := trimSeqIDs(seqIDs)

	fasta.Close()
	OPENFILESCHAN <- true

	waiting.Add(1)
	go scanHMMFolder(
		waiting,
		HMMERRESULTREF,
		&HMMREFINSTANCE,
		seqIDsTrimmed,
		".*.txt",
		"HMMER REFERENCE sequences")

	refClMapping := getRefclIDMapping(seqIDs)

	CLUSTERREF = utils.MCLResults{}
	CLUSTERREF.Init("")
	CLUSTERREF.ConstrutSeqIDtoClIDFromExternalMapping(refClMapping)

	tDiff := time.Since(tStart)
	fmt.Printf("Domain clusters for reference sequences done in time: %f s \n", tDiff.Seconds())
}


func trimSeqIDs(seqIDs map[string]bool) (trimmedSeqs map[string]bool) {
	trimmedSeqs  = make(map[string]bool)

	for seqID := range seqIDs {
		trimmed := strings.SplitN(seqID, " ", 2)[0]
		trimmedSeqs[trimmed] = true
	}

	return trimmedSeqs
}

func getRefclIDMapping(seqIDs map[string] bool) (refClMapping map[string]string) {

	refClMapping = make(map[string]string)

	var seqID, clID string
	cACLAMEre := regexp.MustCompile(`aclame.*?:[0-9]+\ `)
	cACLAMEre2 := regexp.MustCompile(`family:plasmids:[0-9]+\ `)
	cKEGGre := regexp.MustCompile(`K[0-9]+`)

	var regex * regexp.Regexp

	fmt.Printf("Creating reference cluster object...\n")
	for seqID = range seqIDs {

		switch {
		case strings.Count(seqID, "family:plasmids:") > 0:
			regex = cACLAMEre2
		case strings.Count(seqID, ":plasmid") > 0:
			regex = cACLAMEre
		default:
			regex = cKEGGre
		}


		seqID = strings.TrimLeft(seqID, ">")
		clID = strings.Trim(regex.FindString(seqID), " ")

		if clID == "" {
			panic(fmt.Sprintf("Impossible to find clID (regex:%s) from %s",
				regex.String(), seqID))
		}

		seqID = strings.SplitN(seqID, " ", 2)[0]
		refClMapping[seqID] = clID
	}

	return refClMapping
}


func scanHMMFolder (
	waiting * sync.WaitGroup,
	hmmerResult utils.Foldername,
	hmmInstance * utils.HmmerResults,
	seqIDs map[string]bool,
	regex string,
	printLog string) {

	defer waiting.Done()

	waiting.Add(1)
	hmmerResult.ListFiles(regex, waiting)

	mutex := sync.Mutex{}

	nbFiles := hmmerResult.GetNbFiles()

	fmt.Printf("#### Scanning %d HMMER files...\n", nbFiles)

	for index, name := range hmmerResult.GetFileNames() {
		matchingFile, isFound := hmmerResult.GetFilePath(name)

		if !isFound {
			panic(fmt.Sprintf("HMMer file not found %s", name))
		}

		guard := make(chan bool, NBTHREADS)

		for i := 0 ;i < NBTHREADS;i++ {
			guard <- true
		}

		waiting.Add(1)
		<- guard
		go scanOneHMMFile(
			matchingFile,
			waiting,
			guard,
			hmmInstance,
			&mutex,
			seqIDs)

		if printLog != "" {
			fmt.Printf("\r %s processed files (%d / %d )     ",
				printLog, index, nbFiles)
		}
	}

	if printLog != "" {
		fmt.Printf("\n")
	}
}

func scanOneHMMFile(
	file string,
	waiting * sync.WaitGroup,
	guard chan bool,
	hmmInstance * utils.HmmerResults,
	mutex * sync.Mutex,
	seqIDs map[string]bool) {

	defer waiting.Done()
	hmm := utils.HmmerResults{}

	<-OPENFILESCHAN
	hmm.Init(file, false)
	hmm.CreateDomainsPerSeqMapping(seqIDs)
	hmm.Close()
	OPENFILESCHAN <- true

	mutex.Lock()
	hmmInstance.Update(hmm)
	mutex.Unlock()

	guard <- true
}
