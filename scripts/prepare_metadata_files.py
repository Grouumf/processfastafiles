import pandas as pd


########################## VARIABLE ################################
META = "/home/opoirion/data/RECE/Replicon_dataset2020.csv"
PATH = "/home/opoirion/data/RECE/"
####################################################################


def main():
    """ """
    frame = pd.read_csv(META, index_col=0, sep=";")

    frame['L_bp'].to_csv("{0}/replicons.size".format(PATH),
                         sep="\t", header=False)
    frame['Class'].to_csv("{0}/replicons.class".format(PATH),
                          sep="\t", header=False)
    frame['Phylum'].to_csv("{0}/replicons.phylum".format(PATH),
                           sep="\t", header=False)
    frame['Type'].to_csv("{0}/replicons.type".format(PATH),
                         sep="\t", header=False)

if __name__ == '__main__':
    main()
