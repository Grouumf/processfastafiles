python3 scripts/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o CLASSIF_analysis/chr_vs_pl.ANNOTATION.v3.tsv \
        -a 1 \
        --exclude_type "SER" \
        --corr_variable Class \
        -an KO_ACLAME_annotation.tsv

python3 scripts/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o CLASSIF_analysis/chr_vs_ser.ANNOTATION.v3.tsv \
        -a 1 \
        --corr_variable Class \
        --exclude_type "PLD" \
        -an KO_ACLAME_annotation.tsv

python3 scripts/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o CLASSIF_analysis/pld_vs_ser.ANNOTATION.v3.tsv \
        -a 1 \
        --corr_variable Class \
        --exclude_type "CHR" \
        -an KO_ACLAME_annotation.tsv


python3 scripts/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.domain.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o CLASSIF_analysis/chr_vs_pl.DOMAIN.v3.tsv \
        -a 1 \
        --corr_variable Class \
        --exclude_type "SER" \
        -an KO_ACLAME_annotation.tsv

python3 scripts/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.domain.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o CLASSIF_analysis/chr_vs_ser.DOMAIN.v3.tsv \
        -a 1 \
        --corr_variable Class \
        --exclude_type "PLD" \
        -an KO_ACLAME_annotation.tsv

python3 scripts/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.domain.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o CLASSIF_analysis/pld_vs_ser.DOMAIN.v3.tsv \
        -a 1 \
        --corr_variable Class \
        --exclude_type "CHR" \
        -an KO_ACLAME_annotation.tsv
