import pandas as pd

import numpy as np

from scipy.sparse import coo_matrix

from argparse import ArgumentParser

from collections import defaultdict


ARGPARSER = ArgumentParser(
    description='Format matrix to dense and tsv file',
    prefix_chars='-')


ARGPARSER.add_argument('-m', '--coo',
                       required=True,
                       help='COO matrix',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-o', '--out',
                       required=True,
                       help='out file',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-f', '--metadata',
                       required=True,
                       help='metadata file',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-an', '--annotation',
                       help='file containing annotation (featureID<tab>Annotation) Optional',
                       type=str,
                       default="",
                       metavar='file')

ARGS = ARGPARSER.parse_args()

def main():
    """ """

    annotations = defaultdict(lambda: x)

    if ARGS.annotation:
        for line in open(ARGS.annotation):
            line = line.strip()

            if not line:
                continue

            split = line.split('\t')
            annotations[split[0]] = "{0}:{1}".format(split[0], split[1])

    matrix, index_x, index_y = return_matrix()

    index_x_r = {j: i for i, j in index_x.items()}
    index_y_r = {j: i for i, j in index_y.items()}

    xgis = np.array([index_x_r[x] for x in np.arange(matrix.shape[0])])
    ygis = np.array([index_y_r[x] for x in np.arange(matrix.shape[1])])

    if annotations:
        ygis = np.array([annotations[x] for x in ygis])

    frame = pd.read_csv(ARGS.metadata, index_col=0, sep=";")
    matrix_frame = pd.DataFrame(matrix.todense(), columns=ygis, index=xgis)

    matrix_frame = matrix_frame.merge(
        frame,
        left_index=True,
        right_index=True
    )

    matrix_frame.to_csv(ARGS.out, sep="\t")
    print("file created: {0}".format(ARGS.out))


def return_matrix():
    """ """

    matrix = pd.read_csv(ARGS.coo, header=None, sep="\t")

    index_x = {k: i for i, k in enumerate(set(matrix[0]))}
    index_y = {k: i for i, k in enumerate(set(matrix[1]))}

    xarr = np.array([index_x[k] for k in matrix[0]])
    yarr = np.array([index_y[k] for k in matrix[1]])

    sparse_matrix = coo_matrix((matrix[2], (xarr, yarr))).tocsr()

    return sparse_matrix, index_x, index_y


if __name__ == '__main__':
    main()
