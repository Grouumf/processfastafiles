from argparse import ArgumentParser

import pandas as pd

import numpy as np

from umap import UMAP

from MulticoreTSNE import MulticoreTSNE as TSNE

from sklearn.manifold import MDS

from time import time

from sklearn.feature_extraction.text import TfidfTransformer

from sklearn.decomposition import TruncatedSVD

from scipy.sparse import coo_matrix


ARGPARSER = ArgumentParser(
    description='Create UMAP for RECE',
    prefix_chars='-')


ARGPARSER.add_argument('-m', '--coo',
                       required=True,
                       help='COO matrix',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-o', '--out',
                       required=True,
                       help='out file',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-n', '--nb_dims',
                       required=False,
                       help='SVD dim',
                       type=int,
                       default=25,
                       metavar='int')

ARGPARSER.add_argument('-ncu', '--nb_umap_embeddings',
                       required=False,
                       help='Number of UMAP embeddings (default 2)',
                       type=int,
                       default=2,
                       metavar='int')

ARGPARSER.add_argument('-d', '--metric',
                       required=False,
                       help='Metric to use for UMAP',
                       type=str,
                       default="euclidean",
                       metavar='metric')

ARGPARSER.add_argument('-md', '--min_dist',
                       required=False,
                       help=' UMAP min_dist parameter (default 0.1)',
                       type=float,
                       default=0.1,
                       metavar='float')

ARGPARSER.add_argument('-rs', '--repulsion_strength',
                       required=False,
                       help=' UMAP repulsion_strength parameter (default 2.0)',
                       type=float,
                       default=2.0,
                       metavar='float')


ARGS = ARGPARSER.parse_args()


def main():
    """ """
    matrix, index_x, index_y = return_matrix()

    #### TFIDF ####
    print("#### COmputing TF-IDF...")
    tfidf = TfidfTransformer()

    t_start = time()
    tfidf_matrix = tfidf.fit_transform(matrix)
    print("TFIDF done in {0} s".format(time() - t_start))

    svd = TruncatedSVD(n_components=ARGS.nb_dims)

    #### SVD ####
    print("#### computing SVD...")
    t_start = time()
    embedding = svd.fit_transform(tfidf_matrix)
    print("SVD done in {0} s".format(time() - t_start))


    #### UMAP ####
    print("#### Computing UMAP...")
    umap = UMAP(n_components=ARGS.nb_umap_embeddings,
                metric=ARGS.metric,
                min_dist=ARGS.min_dist,
                repulsion_strength=ARGS.repulsion_strength)
    umap = MDS()

    t_start = time()
    umap_embedding = umap.fit_transform(embedding)
    print("UMAP done in {0} s".format(time() - t_start))

    print("#### Saving results...")
    index_x_r = {j: i for i,j in index_x.items()}
    xgis = np.array([index_x_r[x] for x in np.arange(matrix.shape[0])])

    frame = pd.DataFrame(umap_embedding, index=xgis)
    frame.to_csv(ARGS.out, sep="\t", header=False)

    print("File created: {0}".format(ARGS.out))

def return_matrix():
    """ """

    matrix = pd.read_csv(ARGS.coo, header=None, sep="\t")

    index_x = {k: i for i, k in enumerate(set(matrix[0]))}
    index_y = {k: i for i, k in enumerate(set(matrix[1]))}

    xarr = np.array([index_x[k] for k in matrix[0]])
    yarr = np.array([index_y[k] for k in matrix[1]])

    sparse_matrix = coo_matrix((matrix[2], (xarr, yarr))).tocsr()

    return sparse_matrix, index_x, index_y


if __name__ == '__main__':
    main()
