python3 scripts/mixed_model_size.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o size_REGRESSION_analysis/CHR_size.ANNOTATION.v3.tsv \
        -a 1 \
        --selected_type "CHR" \
        --corr_variable Class \
        -an \
        KO_ACLAME_annotation.tsv \
        -fdr 0.01

python3 scripts/mixed_model_size.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o size_REGRESSION_analysis/PLD_size.ANNOTATION.v3.tsv \
        -a 1 \
        --selected_type "PLD" \
        --corr_variable Class \
        -an KO_ACLAME_annotation.tsv \
        -fdr 0.01

python3 scripts/mixed_model_size.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o size_REGRESSION_analysis/SER_size.ANNOTATION.v3.tsv \
        -a 1 \
        --selected_type "SER" \
        --corr_variable Class \
        -an KO_ACLAME_annotation.tsv \
        -fdr 0.01

################ DOMAIN ################################################
python3 scripts/mixed_model_size.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.domain.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o size_REGRESSION_analysis/CHR_size.DOMAIN.v3.tsv \
        -a 1 \
        --selected_type "CHR" \
        --corr_variable Class \
        -an \
        KO_ACLAME_annotation.tsv \
        -fdr 0.01

python3 scripts/mixed_model_size.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.domain.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o size_REGRESSION_analysis/PLD_size.DOMAIN.v3.tsv \
        -a 1 \
        --selected_type "PLD" \
        --corr_variable Class \
        -an KO_ACLAME_annotation.tsv \
        -fdr 0.01

python3 scripts/mixed_model_size.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.domain.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o size_REGRESSION_analysis/SER_size.DOMAIN.v3.tsv \
        -a 1 \
        --selected_type "SER" \
        --corr_variable Class \
        -an KO_ACLAME_annotation.tsv \
        -fdr 0.01
