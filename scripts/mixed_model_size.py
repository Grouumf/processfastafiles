from argparse import ArgumentParser

import pandas as pd

from scipy.sparse import coo_matrix

import statsmodels.api as sm

import numpy as np

from statsmodels.tools.tools import add_constant

from collections import defaultdict

from statsmodels.stats.multitest import fdrcorrection


# from sklearn.preprocessing import LabelBinarizer

# from scipy import hstack

# from sklearn.preprocessing import minmax_scale

from sys import stdout


ARGPARSER = ArgumentParser(
    description='mixed model size regression feature analysis',
    prefix_chars='-')


ARGPARSER.add_argument('-m', '--coo',
                       required=True,
                       help='COO matrix',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-o', '--out',
                       required=True,
                       help='out file',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-f', '--metadata',
                       required=True,
                       help='metadata file',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-v', '--corr_variable',
                       help='dependant variable used a cofonding factor',
                       type=str,
                       default="Phylum",
                       metavar='key')

ARGPARSER.add_argument('-a', '--alpha',
                       help='alpha for regularization',
                       type=float,
                       default=0.1,
                       metavar='float')

ARGPARSER.add_argument('-e', '--selected_type',
                       required=False,
                       help='selected type',
                       default="PLD",
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-fdr', '--false_discovery_rate',
                       help='False discovery rate (benjamini hochberg)',
                       type=float,
                       default=0.05,
                       metavar='float')

ARGPARSER.add_argument('-an', '--annotation',
                       help='file containing annotation (featureID<tab>Annotation) Optional',
                       type=str,
                       default=0.05,
                       metavar='file')

ARGPARSER.add_argument('-s', '--sep',
                       required=False,
                       help='Separator used for loading the database',
                       type=str,
                       metavar='str',
                       default=";",
                      )


ARGS = ARGPARSER.parse_args()


def main():
    """ """
    annotations = {}

    if ARGS.annotation:
        for line in open(ARGS.annotation):
            line = line.strip()

            if not line:
                continue

            split = line.split('\t')
            annotations[split[0]] = "{0}:{1}".format(split[0], split[1])

    frame = pd.read_csv(ARGS.metadata, index_col=0, sep=ARGS.sep)

    frame = frame[frame["Type"] == ARGS.selected_type]

    matrix, index_x, index_y = return_matrix()

    index_x_r = {j: i for i, j in index_x.items()}
    xgis = np.array([index_x_r[x] for x in np.arange(matrix.shape[0])])

    results = defaultdict(list)
    i = 0

    for feature, index in index_y.items():
        i += 1
        vector = matrix.T[index]
        vector = np.asarray(vector.todense())[0]

        feat_frame = pd.DataFrame(vector, index=xgis, columns=[feature])

        frame_merged = frame.merge(feat_frame,
                                   left_index=True,
                                   right_index=True)

        if frame_merged[feature].sum() == 0:
                print("Feature: {0} null continue...".format(feature))
                continue

        frame_ready = frame_merged[[feature]]
        frame_ready = add_constant(frame_ready)

        try:
            mixed_reg = sm.MixedLM(
                np.log(frame_merged['L_bp']),
                frame_ready,
                groups=frame_merged[ARGS.corr_variable]
            ).fit()

        except Exception as e:
            print("#### Exception: {0}".format(e))
            raise(e)
            continue

        pval = mixed_reg.pvalues[feature]
        tval = mixed_reg.tvalues[feature]

        if not tval or np.isnan(tval):
            pval = 1.0
            tval = 0.0

        featureID = feature

        if feature in annotations:
            featureID = annotations[feature]

        results["feature"].append(featureID)
        results["pvalue"].append(pval)
        results["tvalue"].append(tval)

        stdout.write("\r #### ({0} / {1}) features: {2} pval: {3}         " \
                     .format(
                         i, len(index_y), feature, mixed_reg.pvalues[feature]
                     ))
        stdout.flush()

    results_frame = pd.DataFrame(results)

    pvalues = results_frame["pvalue"]

    evalues, fdrpassed = fdrcorrection(pvalues, alpha=ARGS.false_discovery_rate)
    results_frame["evalue"] = evalues
    results_frame["FDR (BH={0})".format(ARGS.false_discovery_rate)] = fdrpassed

    results_frame = results_frame.sort_values(by=["tvalue"], ascending=False)
    results_frame.to_csv(ARGS.out, sep="\t", index=False)

    print("\n#### File written: {0}".format(ARGS.out))


def return_matrix():
    """ """

    matrix = pd.read_csv(ARGS.coo, header=None, sep="\t")

    index_x = {k: i for i, k in enumerate(set(matrix[0]))}
    index_y = {k: i for i, k in enumerate(set(matrix[1]))}

    xarr = np.array([index_x[k] for k in matrix[0]])
    yarr = np.array([index_y[k] for k in matrix[1]])

    sparse_matrix = coo_matrix((matrix[2], (xarr, yarr))).tocsr()

    return sparse_matrix, index_x, index_y


if __name__ == '__main__':
    main()
