PATH_SCRIPT="/home/opoirion/data/RECE/scripts"

for size in  25000 100000 200000 350000 500000 700000
do
    echo "#### size ${size}"
    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/pld.${size}.v3.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i PLD \
            --corr_variable Class \
            -t ${size}

    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.domain.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/pld.${size}.domain.v3.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i PLD \
            --corr_variable Class \
            -t ${size}
done


for size in  350000 500000 700000
do
    echo "#### size ${size}"
    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/SER.${size}.v3.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i SER \
            --corr_variable Class \
            -t ${size}

    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.domain.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/SER.${size}.domain.v3.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i SER \
            --corr_variable Class \
            -t ${size}
done


####### SER vs PL CLASSIF ####################################################
python3 ${PATH_SCRIPT}/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o ./SIZE_analysis/PLD_vs_SER.max_350kb.v3.tsv \
        -a 1 \
        -an KO_ACLAME_annotation.tsv \
        --corr_variable Class \
        -e CHR \
        -mS 350000

python3 ${PATH_SCRIPT}/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o ./SIZE_analysis/PLD_vs_SER.min_350kb.max_500kb.v3.tsv \
        -a 1 \
        -an KO_ACLAME_annotation.tsv \
        --corr_variable Class \
        -e CHR \
        -ms 350000 \
        -mS 500000

python3 ${PATH_SCRIPT}/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o ./SIZE_analysis/PLD_vs_SER.min_500kb.max_700kb.v3.tsv \
        -a 1 \
        -an KO_ACLAME_annotation.tsv \
        --corr_variable Class \
        -e CHR \
        -ms 500000 \
        -mS 700000

python3 ${PATH_SCRIPT}/logistic_Reg_feature_analysis.py \
        -m AllvsAllReducedBlastResults.I.1.4.out.clAnnotation.coo.mat.gz \
        -f Replicon_dataset2020.csv \
        -o ./SIZE_analysis/PLD_vs_SER.min_700kb.v3.tsv \
        -a 1 \
        -an KO_ACLAME_annotation.tsv \
        --corr_variable Class \
        -e CHR \
        -ms 700000

################################################################################

####### PL size  INTERVAL ####################################################
