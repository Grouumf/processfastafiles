PATH_SCRIPT="/home/opoirion/data/RECE/scripts"


for matType in "clAnnotation" "domain"
do
    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.${matType}.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/pld.200000.max_size_350000.${matType}.v2.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i PLD \
            -mS 350000 \
            -t 200000

    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.${matType}.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/pld.350000.max_size_500000.${matType}.v2.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i PLD \
            -mS 500000 \
            -t 350000

    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.${matType}.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/pld.500000.max_size_700000.${matType}.v2.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i PLD \
            -mS 700000 \
            -t 500000
done

r matType in "clAnnotation" "domain"
do
    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.${matType}.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/SER.350000.max_size_500000.${matType}.v2.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i SER \
            -mS 500000 \
            -t 350000

    python3 ${PATH_SCRIPT}/logistic_Reg_size_class_analysis.py \
            -m AllvsAllReducedBlastResults.I.1.4.out.${matType}.coo.mat.gz \
            -f Replicon_dataset2020.csv \
            -o ./SIZE_analysis/SER.500000.max_size_700000.${matType}.v2.tsv \
            -a 1 \
            -an KO_ACLAME_annotation.tsv \
            -i SER \
            -mS 700000 \
            -t 500000
done
