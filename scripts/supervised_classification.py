from argparse import ArgumentParser

import pandas as pd

from scipy.sparse import coo_matrix

from collections import Counter
from collections import defaultdict

import numpy as np

from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import precision_recall_fscore_support

import warnings

from sys import stdout

import seaborn as sns
import pylab as plt


ARGPARSER = ArgumentParser(
    description='Classif analysis',
    prefix_chars='-')


ARGPARSER.add_argument('-m', '--coo',
                       required=True,
                       help='COO matrix',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-o', '--out',
                       required=True,
                       help='out file',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-f', '--metadata',
                       required=True,
                       help='metadata file',
                       type=str,
                       metavar='file')

ARGPARSER.add_argument('-an', '--annotation',
                       help='file containing annotation (featureID<tab>Annotation) Optional',
                       type=str,
                       default=0.05,
                       metavar='file')

ARGPARSER.add_argument('-tk', '--sampling_key',
                       required=False,
                       help='Key used to balance the sampling from the metadata and equally represent each type (default: "Phylum1")',
                       type=str,
                       default="Phylum1",
                       metavar='str')

ARGPARSER.add_argument('-lk', '--label_key',
                       required=False,
                       help='Key used to assign classification labels to each sample (default: "Type")',
                       type=str,
                       default="Type",
                       metavar='str')

ARGPARSER.add_argument('-ss', '--sample_size',
                       required=False,
                       help='sample size used for each classifier',
                       type=int,
                       metavar='int',
                       default=1000,
                      )

ARGPARSER.add_argument('-nbc', '--classifiers_nb',
                       required=False,
                       help='sample size used for each classifier',
                       type=int,
                       metavar='int',
                       default=100,
                      )

ARGPARSER.add_argument('-s', '--show',
                       help='Display figure',
                       action="store_true")

ARGPARSER.add_argument('-fs', '--font_scale',
                       required=False,
                       help='seaborn font scale',
                       type=float,
                       metavar='float',
                       default=0.8,
                      )


ARGS = ARGPARSER.parse_args()


sns.set(font_scale=ARGS.font_scale)

def main():
    """ """
    matrix, index_x, index_y = return_matrix()

    annotations = {}

    if ARGS.annotation:
        for line in open(ARGS.annotation):
            line = line.strip()

            if not line:
                continue

            split = line.split('\t')
            annotations[split[0]] = split[1]

    frame = pd.read_csv(ARGS.metadata, index_col=0, sep=";")

    index_weight, index_type, labels, xgis = create_index_weight(frame, index_x)
    index_to_rm = labels == '0.0'

    matrix = matrix[~index_to_rm]
    index_weight = index_weight[~index_to_rm]
    index_type = index_type[~index_to_rm]
    labels = labels[~index_to_rm]
    xgis = xgis[~index_to_rm]

    dfr, dfo = train_classifier(
        matrix, index_type, index_weight, labels, xgis)

    dfr = dfr.join(frame, how='left')
    dfr.to_csv(ARGS.out, sep="\t")
    print("file written: {0}".format(ARGS.out))

    out2 = "{0}.performance.tsv".format(ARGS.out.rsplit('.', 1)[0])

    dfo.to_csv(out2, sep="\t")
    print("file written: {0}".format(out2))


    fig, axes = plt.subplots(3, 1, figsize=(16, 12))
    bplot = sns.barplot(
        data=dfr,
        y="proba",
        x="ClassID",
        hue="annotation",
        order=sorted(set(dfr['ClassID'])),
        ax=axes[0])

    bplot.set_xticklabels(bplot.get_xticklabels(), rotation=30)
    bplot.set_title('Mean probability per class / annotation')

    gdf = dfr.loc[:, ['same_label', 'ClassID', 'label']].groupby(['ClassID', 'label']).mean()
    gdf = gdf.reset_index()
    bplot = sns.barplot(
        data=gdf,
        y="same_label",
        x="ClassID",
        order=sorted(set(gdf['ClassID'])),
        hue="label",
        ax=axes[1])

    bplot.set_xticklabels(bplot.get_xticklabels(), rotation=30)
    bplot.set_title('Accuracy toward provided label ')

    bplot = sns.barplot(
        data=dfo,
        y="Score",
        x="Key",
        hue="Type",
        ax=axes[2])
    bplot.set_title('Average classifier performance on outer bag samples')

    plt.tight_layout()

    out3 = "{0}.performance.pdf".format(ARGS.out.rsplit('.', 1)[0])
    fig.savefig(out3)
    print("figure created: {0}".format(out3))

    if ARGS.show:
        plt.show()


def train_classifier(matrix, index_type, index_weight, labels, xgis):
    """
    """
    print("Training classifiers...")
    parameters = {'kernel':('linear', 'rbf'), 'C':[0.1, 1, 10, 100]}
    svc = SVC()
    clf = GridSearchCV(svc, parameters, cv=3, n_jobs=4)
    clf = ExtraTreesClassifier()

    res_labels = defaultdict(Counter)
    res_overall = defaultdict(list)

    for i in range(ARGS.classifiers_nb):
        index = np.random.choice(
            np.arange(index_weight.shape[0]),
            replace=False,
            p=index_weight, size=ARGS.sample_size)
        index_c = np.ones(matrix.shape[0]).astype('bool')
        index_c[index] = False

        matrix_train = matrix[index]
        labels_train = labels[index]

        matrix_test = matrix[index_c]
        labels_test = labels[index_c]
        weight_test = index_weight[index_c]
        xgi_test = xgis[index_c]

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            clf.fit(matrix_train, labels_train)

        stdout.write("\r iteration: {0} / {1}    ".format(
            i, ARGS.classifiers_nb))

        labels_predicted = clf.predict(matrix_test)

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            res = precision_recall_fscore_support(
                labels_predicted, labels_test, sample_weight=weight_test)

        keys = sorted(set(labels_test))

        for i, kk in enumerate(['Precision', 'Recall', 'F-score']):
            res_overall['Score'] += res[i].tolist()
            res_overall['Key'] += keys
            res_overall['Type'] += [kk for _ in res[0]]

        for xgi, label in zip(xgi_test, labels_predicted):
            res_labels[xgi][label] += 1

    print("\n")

    res_labels_final = {}
    true_labels_dict = {x:l for x, l in zip(xgis, labels)}
    type_dict = {x:l for x, l in zip(xgis, index_type)}

    for key, count in res_labels.items():
        tot = sum(count.values())
        label, val = count.most_common()[0]
        res_labels_final[key] = [label, val / tot, true_labels_dict[key], type_dict[key]]

    dfr = pd.DataFrame(res_labels_final).T
    dfr.columns = ['label', 'proba', 'annotation', 'ClassID']

    dfo = pd.DataFrame(res_overall)

    dfr['same_label'] = dfr['label'] == dfr['annotation']

    return dfr, dfo

def create_index_weight(frame, index_x):
    """
    """
    print("Creating index...")
    index_weight = np.zeros(len(index_x))
    index_type = np.zeros(len(index_x)).astype('str')
    labels = np.zeros(len(index_x)).astype('str')
    xgis = np.zeros(len(index_x)).astype('str')
    index_set = set(frame.index)
    prop = Counter(frame[ARGS.sampling_key])

    tot = sum(prop.values())

    for x, p in index_x.items():
        if x not in index_set:
            continue

        sample = frame.loc[x, :]

        index_weight[p] = tot / prop[sample[ARGS.sampling_key]]
        index_type[p] = sample[ARGS.sampling_key]
        labels[p] = sample[ARGS.label_key]
        xgis[p] = x

    index_weight /= index_weight.sum()

    return index_weight, index_type, labels, xgis

def return_matrix():
    """ """

    matrix = pd.read_csv(ARGS.coo, header=None, sep="\t")

    index_x = {k: i for i, k in enumerate(set(matrix[0]))}
    index_y = {k: i for i, k in enumerate(set(matrix[1]))}

    xarr = np.array([index_x[k] for k in matrix[0]])
    yarr = np.array([index_y[k] for k in matrix[1]])

    sparse_matrix = coo_matrix((matrix[2], (xarr, yarr))).tocsr()

    return sparse_matrix, index_x, index_y

if __name__ == '__main__':
    main()
