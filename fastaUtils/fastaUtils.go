package fastautils

import (
	"fmt"
	"os"
	"io"
	"io/ioutil"
	utils "gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils"
	"strings"
	"path"
	"bufio"
	"bytes"
	"strconv"
	"sync"
	"math/rand"
	"time"
	"math"
	"regexp"
	"github.com/mroth/weightedrand"
)

/*FolderListFlags folder list for flag */
type FolderListFlags []*Foldername

/*String ... */
func (i *FolderListFlags) String() string {
	var str string;
	for _, s := range (*i) {
		str = str + "\t"+  s.Name
	}

	return str
}

/*Set ... */
func (i *FolderListFlags) Set(folderstr string) error {
	folder := Foldername{}
	folder.Set(folderstr)

	*i = append(*i, &folder)
	return nil
}

/*Foldername type used to check if folder exists */
type Foldername struct {
	Name string
	files []string
	index map[string]int
}


/*Set Foldername from string */
func (i *Foldername) Set(filename string) error {
	AssertIfFolderExists(filename)
	(*i).Name =  filename
	(*i).files = make([]string, 0)
	(*i).index = make(map[string]int)
	return nil
}

/*String to string */
func (i *Foldername) String() string {
	return string((*i).Name)
}

/*GetFileNames get files listed */
func (i *Foldername) GetFileNames() []string {
	return (*i).files
}


/*GetNbFiles ... */
func (i *Foldername) GetNbFiles() int {
	return len((*i).files)
}

/*ListFiles list file inside folder */
func (i *Foldername) ListFiles(regex string, waiting *sync.WaitGroup) {
	defer waiting.Done()

	fmt.Printf("# Listing folder: %s\n", string((*i).Name))
	fileinfo, err := ioutil.ReadDir(string((*i).Name))
	utils.Check(err)
	var ext, name, filename string
	var count int

	regexC := regexp.MustCompile(regex)

	for _, file := range fileinfo {
		if file.IsDir() {
			continue
		}
		name = file.Name()

		if regex != "" && regexC.FindString(name) == "" {
			continue
		}

		(*i).files = append((*i).files, name)

		_, filename = path.Split(name)
		ext = path.Ext(filename)

		(*i).index[filename[:len(filename) - len(ext)]] = count
		count++
	}

	if count == 0 {
		panic(fmt.Sprintf(
			"Error no files found for folder: %s (REGEX: %s)\n",
			string((*i).Name), regex))
	}
}

/*GetFilePath get matching file for a folder */
func (i * Foldername) GetFilePath(file string) (matchingFile string, isFound bool) {
	var index int
	_, filename := path.Split(file)
	ext := path.Ext(filename)
	key := filename[:len(filename) - len(ext)]

	if index, isFound = (*i).index[key];isFound {
		matchingFile = path.Join((*i).Name, (*i).files[index])
	}

	return matchingFile, isFound
}

/*AssertIfFolderExists panic if folder doesnt exists */
func AssertIfFolderExists(foldername string) {
	stat, err := os.Stat(foldername)

	if err==nil && !stat.IsDir() {
		panic(fmt.Sprintf("folder: %s is a file", foldername))
	} else if err != nil {
		panic(fmt.Sprintf("!!!!Error %s with folder: %s. Doesn't seems to exists\n",
			err, foldername))
	}
}

/*MCLResults MCL results structure */
type MCLResults struct {
	filename utils.Filename
	file *os.File
	reader *bufio.Reader
	buffer bytes.Buffer
	clSizeMap map[int]int
	domainCountMap map[string]int
	seqIDtoClID map[string]int
	ClusterDomainsVector []map[string]float64
	clNameToClID map[string]int
	domainWeightedRand * weightedrand.Chooser
	nbDomainWeightedRand * weightedrand.Chooser
	clIDWeightedRand * weightedrand.Chooser
	clIDtoSeqIDs map[int][]string
	nbDomainsPerSeq map[int]int
	ClIDToClName map[int]string
}

/*Init init Mclresults */
func (mcl *MCLResults) Init(filename string) {

	if filename != "" {
		(*mcl).filename = utils.Filename(filename)
		_, file := (*mcl).filename.ReturnReader(0)
		(*mcl).file = file
		(*mcl).reader = bufio.NewReader(file)
	}

	(*mcl).buffer = bytes.Buffer{}
	(*mcl).clSizeMap = make(map[int]int)
	(*mcl).seqIDtoClID = make(map[string]int)
	(*mcl).clNameToClID = make(map[string]int)
	(*mcl).ClIDToClName = make(map[int]string)
	(*mcl).domainCountMap = make(map[string]int)
	(*mcl).clIDtoSeqIDs = make(map[int][]string)
	(*mcl).nbDomainsPerSeq = make(map[int]int)
}

/*GetClSizeMap get cl size map */
func (mcl *MCLResults) GetClSizeMap () (map[int]int) {
	return (*mcl).clSizeMap
}


/*GetAllSeqIDsfromClID get all Seq IDs from clIDref */
func (mcl *MCLResults) GetAllSeqIDsfromClID (clIDref int) (seqIDs []string) {
	var isInside bool

	if seqIDs, isInside = (*mcl).clIDtoSeqIDs[clIDref];!isInside {
		panic(fmt.Sprintf("Cluster ID: %d not in MCL class index: %s",
			clIDref, (*mcl).filename))
	}

	return seqIDs
}


/*WriteSeqsFromClIDstoBuffer ...  */
func (mcl *MCLResults) WriteSeqsFromClIDstoBuffer (clIDs []int) {
	var isInside bool
	var seqIDs []string
	var seqID string
	var clID int

	for _, clID = range clIDs {

		if seqIDs, isInside = (*mcl).clIDtoSeqIDs[clID];!isInside {
			panic(fmt.Sprintf("Cluster ID: %d not in MCL class index: %s",
				clID, (*mcl).filename))
		}

		for _, seqID = range seqIDs {
			(*mcl).buffer.WriteString(seqID)
			(*mcl).buffer.WriteRune('\t')
			(*mcl).buffer.WriteString(strconv.Itoa(clID))
			(*mcl).buffer.WriteRune('\n')
		}

	}
}

// initWeightedRands
func (mcl *MCLResults) initWeightedRands () {

	var choices, dChoices, cChoices []weightedrand.Choice

	rand.Seed(time.Now().UTC().UnixNano())

	if len((*mcl).nbDomainsPerSeq) == 0 {
		panic("nbDomainsPerSeq empty!!! cannot initialiwe random weight")
	}

	for nbDomains, count := range (*mcl).nbDomainsPerSeq {
		choices = append(choices, weightedrand.Choice{Item: nbDomains, Weight: uint(count)})
	}

	(*mcl).nbDomainWeightedRand, _ = weightedrand.NewChooser(choices...)

	if len((*mcl).domainCountMap) == 0 {
		panic(fmt.Sprintf("domainCountMap empty!!! cannot initialiwe random weight"))
	}

	for domain, count := range (*mcl).domainCountMap {
		dChoices = append(dChoices, weightedrand.Choice{Item: domain, Weight: uint(count)})
	}

	(*mcl).domainWeightedRand, _ = weightedrand.NewChooser(dChoices...)

	for clID, clSize := range (*mcl).clSizeMap {
		cChoices = append(cChoices, weightedrand.Choice{Item: clID, Weight: uint(clSize)})
	}

	(*mcl).clIDWeightedRand, _ = weightedrand.NewChooser(cChoices...)
}

/*CreateDomainClusterVectors create a domain fector normed by CL size for each cluster */
func (mcl *MCLResults) CreateDomainClusterVectors(seqIDToDomains map[string][]string) {
	var isInside bool
	var clusterID int
	var clusterSize float64
	var sequence, domain string

	(*mcl).ClusterDomainsVector = make([]map[string]float64, len((*mcl).clSizeMap))

	for i := range (*mcl).clSizeMap {
		(*mcl).ClusterDomainsVector[i] = make(map[string]float64)
	}

	for sequence = range seqIDToDomains {
		if clusterID, isInside = (*mcl).seqIDtoClID[sequence];!isInside {
			(*mcl).nbDomainsPerSeq[0]++
			continue
		}

		clusterSize = float64((*mcl).clSizeMap[clusterID])

		for _, domain = range seqIDToDomains[sequence] {
			(*mcl).ClusterDomainsVector[clusterID][domain] += 1.0 / clusterSize
			(*mcl).domainCountMap[domain]++
		}

		nbDomains := len(seqIDToDomains[sequence])

		(*mcl).nbDomainsPerSeq[nbDomains]++
	}

	(*mcl).initWeightedRands()
}


/*ConstrutSeqIDtoClIDFromExternalMapping instantiate cl index using external mapping */
func (mcl * MCLResults) ConstrutSeqIDtoClIDFromExternalMapping(externalMapping map[string]string) {

	var count, clID int

	var isInside bool

	for  seqID, clStrID := range externalMapping {

		if clID, isInside = (*mcl).clNameToClID[clStrID];!isInside {
			clID = count
			(*mcl).clNameToClID[clStrID] = clID
			(*mcl).ClIDToClName[clID] = clStrID
			count++
		}

		(*mcl).clSizeMap[clID]++
		(*mcl).seqIDtoClID[seqID] = clID
		(*mcl).clIDtoSeqIDs[clID] = append((*mcl).clIDtoSeqIDs[clID], seqID)
	}
}


/*ConstrutSeqIDtoClIDMapping write seqID<tab>CL Number per line  */
func (mcl *MCLResults) ConstrutSeqIDtoClIDMapping() (clID int) {
	var split []string
	var seqID string
	var line string
	var err error

	for  {

		line, err = (*mcl).reader.ReadString('\n')

		split = strings.Split(line, "\t")

		for _, seqID = range split {
			seqID = strings.Trim(seqID, "\n ")
			(*mcl).seqIDtoClID[seqID] = clID
			(*mcl).clIDtoSeqIDs[clID] = append((*mcl).clIDtoSeqIDs[clID], seqID)
		}

		(*mcl).clSizeMap[clID] = len((*mcl).clIDtoSeqIDs[clID])

		clID++

		if err == io.EOF {
			break
		}

		utils.Check(err)

	}

	return clID
}

/*WriteBuff write buffer and reset */
func (mcl *MCLResults) WriteBuff() []byte {
	defer (*mcl).buffer.Reset()

	return (*mcl).buffer.Bytes()
}

/*GetMostRepresentedCluster get the most represented cluster
 using a list of query seqs that should all be in seqIDtoClID  */
func (mcl *MCLResults) GetMostRepresentedCluster(seqIDs []string, raiseErr bool) (topCl int,
	topClStr string, ratio, halfEntropy float64) {

	var ClID int
	var count int
	var isInside bool
	var max int

	clCount := make([]int, len((*mcl).clSizeMap))

	for _, seqID := range seqIDs {
		if ClID, isInside = (*mcl).seqIDtoClID[seqID];!isInside {
			if raiseErr {
				panic(fmt.Sprintf("Sequence: %s not in MCL clustering (%s)",
					seqID, (*mcl).filename))
			}
		}

		clCount[ClID]++

		if clCount[ClID] > max {
			max = clCount[ClID]
			topCl = ClID
		}
	}

	ratio = float64(max) / float64(len(seqIDs))

	for ClID, count = range clCount {
		if count > 0 {
			halfEntropy -= float64(count) * math.Log((1 + float64(count)) / float64((*mcl).clSizeMap[ClID]))
		}
	}

	return topCl, (*mcl).ClIDToClName[topCl], ratio, halfEntropy
}


/*ReturnDomainVectorsForRandomClusters Return a random vector based
 on domain composition and */
func (mcl * MCLResults) ReturnDomainVectorsForRandomClusters(clusterSize, nbIt int) (
	randomVectors []map[string]float64) {

	randomVectors = make([]map[string]float64, nbIt)
	var nbDomains int
	var domainType string

	unit := 1.0 / float64(clusterSize)

		for i := 0 ; i < nbIt; i ++ {
			randomVectors[i] = make(map[string]float64)

			for j := 0; j < clusterSize; j++ {

				nbDomains = (*mcl).nbDomainWeightedRand.Pick().(int)

				for k := 0; k < nbDomains;k++ {
					domainType = (*mcl).domainWeightedRand.Pick().(string)

					if domainType == "" {
						panic("Random domain is empty")
					}

					randomVectors[i][domainType] += unit
				}

			}
		}

	return randomVectors
}


/*ReturnDomainVectorsForRandomClustersV2 Return a random vector based
 on domain composition and mixing ref cluster domain with random cl selection */
func (mcl * MCLResults) ReturnDomainVectorsForRandomClustersV2(clIDRef, clusterSize, nbIt int,
	sameClassWeight float64) (
	randomVectors []map[string]float64) {

	if sameClassWeight > 1.0 || sameClassWeight < 0.0 {
		panic(fmt.Sprintf("#### Error! sameClassweight %f should be between 0 and 1\n",
			sameClassWeight))
	}

	randomVectors = make([]map[string]float64, nbIt)
	var nbDomains, clID int
	var domainType string

	var randRefGen * weightedrand.Chooser
	var randGenPtr * weightedrand.Chooser
	var randChoice  []weightedrand.Choice
	var dom string
	var  weight float64

	randChoice = []weightedrand.Choice{}

	for dom, weight = range (*mcl).ClusterDomainsVector[clID] {
		if weight <= 0 {
			continue
		}

		randChoice = append(randChoice, weightedrand.Choice{Item: dom,
			Weight: uint(weight * float64(clusterSize))})
	}

	isRefEmpty := len(randChoice) == 0
	randRefGen, _ = weightedrand.NewChooser(randChoice...)

	unit := 1.0 / float64(clusterSize)

		for i := 0 ; i < nbIt; i ++ {
			randomVectors[i] = make(map[string]float64)

			weight = rand.Float64()

			for j := 0; j < clusterSize; j++ {

				if weight < sameClassWeight {
					if isRefEmpty {
						continue
					}

					randGenPtr = randRefGen
				} else {
					randGenPtr = (*mcl).domainWeightedRand
				}

				nbDomains = (*mcl).nbDomainWeightedRand.Pick().(int)

				for k := 0; k < nbDomains;k++ {
					domainType = (*randGenPtr).Pick().(string)

					if domainType == "" {
						panic("Random domain is empty")
					}

					randomVectors[i][domainType] += unit
				}

			}
		}

	return randomVectors
}


/*HmmerResults struct for HMMER results */
type HmmerResults struct {
	filename utils.Filename
	scanner *bufio.Scanner
	file *os.File
	buffer bytes.Buffer
	raiseErr bool
	SeqIDToDomains map[string][]string
	DomainCounts map[string]int
}

/*Init init HmmerResults */
func (hmm *HmmerResults) Init(filename string, raiseErr bool) {
	(*hmm).raiseErr = raiseErr

	if filename != "" {
		(*hmm).filename = utils.Filename(filename)
		(*hmm).scanner, (*hmm).file = (*hmm).filename.ReturnReader(0)

		(*hmm).scanner.Scan()
		line := (*hmm).scanner.Text()

		for {
			if len(line) == 0 {
				continue
			}

			if line[0] != '#' {
				break
			}

			(*hmm).scanner.Scan()
			line = (*hmm).scanner.Text()
		}

		split := strings.Split(line, " ")

		if len(split) < 19 {
			panic(fmt.Sprintf(
				`#### Error when opening HMM file ####
line: %s from filename: %s doesn't seem to match HMM result\n`,
				line, (*hmm).filename.String()))

		}

		utils.CloseFile((*hmm).file)
		(*hmm).scanner, (*hmm).file = (*hmm).filename.ReturnReader(0)
	}

	(*hmm).buffer = bytes.Buffer{}
	(*hmm).SeqIDToDomains = make(map[string][]string)
	(*hmm).DomainCounts = make(map[string]int)

}


/*Close blast results format type 7 */
func (hmm *HmmerResults) Close() {
	utils.CloseFile((*hmm).file)
}

/*Update update hmm with another hmm results*/
func (hmm *HmmerResults) Update(hmm2 HmmerResults) {
	for seqID, domains := range hmm2.SeqIDToDomains {
		(*hmm).SeqIDToDomains[seqID] = append((*hmm).SeqIDToDomains[seqID], domains...)
	}

	for seqID, count := range hmm.DomainCounts {
		(*hmm).DomainCounts[seqID] += count
	}
}


/*CreateDomainsPerSeqMapping init HmmerResults */
func (hmm *HmmerResults) CreateDomainsPerSeqMapping(seqIDSet map[string]bool) {
	var line, seqID, domain, errStr string
	var split, split2 []string
	var lineCount int

	regexwp := regexp.MustCompile(`\s+`)

	for (*hmm).scanner.Scan() {
		line = (*hmm).scanner.Text()
		lineCount++

		line = regexwp.ReplaceAllString(line, " ")

		if line[0] == '#' {
			continue
		}

		split = strings.Split(line, " ")

		if len(split) < 3 {
			errStr = fmt.Sprintf(
				`#### Error with domain file: %s ####\n
LINE (nb: %d ) cannot be splitted in more than 3: %s`,
				(*hmm).filename, lineCount, line)
			fmt.Printf("%s\n", errStr)

			if (*hmm).raiseErr {
				panic(errStr)
			}

			continue
		}

		seqID, domain = split[0], split[2]
		domain = strings.Trim(domain, " \n")

		split2 = strings.SplitN(seqID, "|", 2)

		if len(split2) == 2 {
			seqID = split2[1]
		}

		if !seqIDSet[seqID] {
			continue
		}

		if len(domain) <= 1 {
			errStr = fmt.Sprintf(
				`#### Error with domain file: %s ####\n
LINE (nb: %d): %s (length: %d) and domain: %s (too short)`,
				(*hmm).filename, lineCount, split, len(split), domain)

			fmt.Printf("%s\n", errStr)

			if (*hmm).raiseErr {
				panic(errStr)
			}

			continue
		}

		(*hmm).SeqIDToDomains[seqID] = append((*hmm).SeqIDToDomains[seqID], domain)
		(*hmm).DomainCounts[domain]++}

}

/*WriteSeqDomainToBuffer write seq domains to buffer */
func (hmm *HmmerResults) WriteSeqDomainToBuffer(seqIDs map[string]bool) {
	refSeq := len(seqIDs) > 0

	for seqID, domains := range (*hmm).SeqIDToDomains {

		if refSeq && !seqIDs[seqID] {
			continue
		}

		(*hmm).buffer.WriteString(seqID)

		for _, domain := range domains {
			(*hmm).buffer.WriteRune('\t')
			(*hmm).buffer.WriteString(domain)
		}
		(*hmm).buffer.WriteRune('\n')
	}
}


/*ReturnDomainMatrix retrun domain matrix */
func (hmm *HmmerResults) ReturnDomainMatrix(seqIDs map[string]bool) (matrix map[string]map[string]int) {
	var split []string
	var genomeID, domain string
	var isInside bool

	matrix = make(map[string]map[string]int)

	refSeq := len(seqIDs) > 0

	for seqID, domains := range (*hmm).SeqIDToDomains {
		if refSeq && !seqIDs[seqID] {
			continue
		}

		split = strings.SplitAfter(seqID, "_")
		genomeID = strings.Trim(split[0] + split[1], "_")

		if _, isInside = matrix[genomeID];!isInside {
			matrix[genomeID] = make(map[string]int)
		}

		for _, domain = range domains {
			matrix[genomeID][domain]++
		}
	}

	return matrix
}


/*BlastResultFmt7 struct to process blast results format type 7*/
type BlastResultFmt7 struct {
	filename utils.Filename
	isInit bool
	scanner *bufio.Scanner
	file *os.File
	buffer bytes.Buffer
	seqIDList map[string]bool
	seqIDTag map[string]string
	nbHits int
	nbQueries int
	QueriesList []string
}

/*Close blast results format type 7 */
func (i *BlastResultFmt7) Close() {
	utils.CloseFile((*i).file)
}

/*Init blast results format type 7*/
func (i *BlastResultFmt7) Init(filename string) {
	(*i).filename = utils.Filename(filename)
	(*i).scanner, (*i).file = (*i).filename.ReturnReader(0)
	(*i).scanner.Scan()
	(*i).seqIDList = make(map[string]bool)
	(*i).seqIDTag = make(map[string]string)
	(*i).nbHits = 0
	(*i).nbQueries = 0
	(*i).QueriesList = []string{}

	firstLine := (*i).scanner.Text()

	for ((*i).scanner.Scan() && firstLine[0] == '#') {
		firstLine = (*i).scanner.Text()
	}

	split := strings.Split(firstLine, "\t")

	if len(split) != 12 {
		panic(fmt.Sprintf("line %s from blast result file: %s cannot be splitted in 12",
			split, filename))
	}

	_, err := strconv.ParseFloat(split[11], 64)

	if err != nil {
		panic(fmt.Sprintf("blast score: %s from line %s from blast result file: %s cannot be parsed into float",
			split[11], split, filename))
	}

	(*i).buffer = bytes.Buffer{}
	(*i).isInit = true
}

/*GetAllSeqIDs get all query seq IDs from the  blast results file*/
func (i *BlastResultFmt7) GetAllSeqIDs() (map[string]bool) {

	split := strings.Split((*i).scanner.Text(), "\t")
	var seqname, line string

	(*i).seqIDList[split[0]] = true

	for (*i).scanner.Scan() {
		line = (*i).scanner.Text()

		if line[0] == '#' {
			continue
		}

		split = strings.Split(line, "\t")

		if len(split) != 12 {
			panic(fmt.Sprintf("line %s from blast result file: %s cannot be splitted in 12",
				split, (*i).filename))
		}

		seqname = strings.SplitN(
			strings.Trim(split[0], ">\n "), " ", 2)[0]
		(*i).seqIDList[seqname] = true
	}

	return (*i).seqIDList
}

/*WriteHitsToBuffers get all query seq IDs from the  blast results file*/
func (i *BlastResultFmt7) WriteHitsToBuffers(maxHit int, scoreThres float64)  {

	nbHits := 0
	var line, seqname, hit string
	var score float64
	var split []string
	var err error
	var continueToNext bool

	for (*i).scanner.Scan() {
		line = (*i).scanner.Text()
		if continueToNext && line[0] != '#' {
			continue
		}

		if line[0] == '#' {
			continueToNext = false
			nbHits = 0
			continue
		}

		split = strings.Split(line, "\t")

		if len(split) != 12 {
			panic(fmt.Sprintf("line %s from blast result file: %s cannot be splitted in 12",
				split, (*i).filename))
		}

		seqname = strings.SplitN(
			strings.Trim(split[0], ">\n "), " ", 2)[0]

		hit = strings.SplitN(
			strings.Trim(split[1], ">\n "), " ", 2)[0]

		score, err = strconv.ParseFloat(split[10], 64)

		if err != nil {
			panic(fmt.Sprintf("#### blast res score conversion error for file: %s line: %s",
				(*i).filename, line))
		}

		score =  - math.Log10(score + 1e-128)

		if score <= scoreThres {
			(*i).nbQueries++
			(*i).QueriesList = append((*i).QueriesList, seqname)
			continueToNext = true
		}

		if maxHit <= nbHits {
			(*i).nbQueries++
			(*i).QueriesList = append((*i).QueriesList, seqname)
			continueToNext = true
		}

		if continueToNext {
			continue
		}

		nbHits++
		(*i).nbHits++

		split = strings.SplitN(seqname, "|", 2)

		if len(split) == 2 {
			seqname = split[1]
		}

		seqname = strings.Split(seqname, "__")[0]

		(*i).buffer.WriteString(seqname)
		(*i).buffer.WriteRune('\t')
		(*i).buffer.WriteString(hit)
		(*i).buffer.WriteRune('\t')
		(*i).buffer.WriteString(strconv.FormatFloat(score, 'f', 2, 64))
		(*i).buffer.WriteRune('\n')
	}

	(*i).nbQueries++
	(*i).QueriesList = append((*i).QueriesList, seqname)
}


/*WriteBuff write buffer and reset */
func (i *BlastResultFmt7) WriteBuff() []byte {
	defer (*i).buffer.Reset()

	return (*i).buffer.Bytes()
}


/*WriteBuff write buffer and reset */
func (hmm *HmmerResults) WriteBuff() []byte {
	defer (*hmm).buffer.Reset()

	return (*hmm).buffer.Bytes()
}

/*GetNbHits get nb hits */
func (i *BlastResultFmt7) GetNbHits() int {
	return (*i).nbHits
}

/*GetNbQueries ... */
func (i *BlastResultFmt7) GetNbQueries() int {
	return (*i).nbQueries
}

/*GetAllSeqIDsWithMatch get all query seq IDs from the  blast results file*/
func (i *BlastResultFmt7) GetAllSeqIDsWithMatch() (map[string]string) {

	split := strings.Split((*i).scanner.Text(), "\t")
	var seqname, matchname, line string

	(*i).seqIDList[split[0]] = true
	var match bool

	var split2 []string

	for (*i).scanner.Scan() {
		line = (*i).scanner.Text()

		if match && line[0] != '#' {
			continue
		}

		if line[0] == '#' {
			match = false
			continue
		}

		split = strings.Split(line, "\t")

		if len(split) != 12 {
			panic(fmt.Sprintf("line %s from blast result file: %s cannot be splitted in 12",
				split, (*i).filename))
		}

		seqname = strings.SplitN(
			strings.Trim(split[0], ">\n "), " ", 2)[0]

		split2 = strings.SplitN(seqname, "|", 2)

		if len(split2) == 2 {
			seqname = split2[1]
		}

		matchname = strings.SplitN(
			strings.Trim(split[1], ">\n "), " ", 2)[0]

		(*i).seqIDTag[seqname] = matchname
		match = true
	}

	return (*i).seqIDTag
}

/*GetTopAndLastKHitsPerRef get top and last K hits for each ref sequence */
func (i *BlastResultFmt7) GetTopAndLastKHitsPerRef(topK, lastK int) (map[string]bool) {

	split := strings.Split((*i).scanner.Text(), "\t")
	var seqname, line string

	(*i).seqIDList[split[0]] = true
	var topSeqs, lastSeqs []string
	var topCount, lastCount int

	topSeqs = make([]string, topK)
	lastSeqs = make([]string, lastK)

	newHit := true

	for (*i).scanner.Scan() {
		line = (*i).scanner.Text()

		if line[0] == '#' {
			topCount = 0
			lastCount = 0
			newHit = true
			continue
		}

		if newHit {

			for _, seqname = range topSeqs {
				(*i).seqIDList[seqname] = true
			}

			for _, seqname = range lastSeqs {
				(*i).seqIDList[seqname] = true
			}

			newHit = false
		}

		split = strings.Split(line, "\t")

		if len(split) != 12 {
			panic(fmt.Sprintf("line %s from blast result file: %s cannot be splitted in 12",
				split, (*i).filename))
		}

		seqname = strings.SplitN(
			strings.Trim(split[0], ">\n "), " ", 2)[0]

		if topK > 0 && topCount < topK {
			topSeqs[topCount] = seqname
			topCount++
		}

		if lastCount >= lastK {
			lastCount = 0
		}

		if lastK > 0 {
			lastSeqs[lastCount] = seqname
			lastCount++
		}
	}

	for _, seqname = range topSeqs {
		(*i).seqIDList[seqname] = true
	}

	for _, seqname = range lastSeqs {
		(*i).seqIDList[seqname] = true
	}

	return (*i).seqIDList
}

/*GetNbSeqs get the number of seqs*/
func (i *BlastResultFmt7) GetNbSeqs() int {
	return len((*i).seqIDList)
}

/*FastaFile struct to process fasta file*/
type FastaFile struct {
	filename utils.Filename
	genomename string
	isInit bool
	currentSeqID string
	scanner *bufio.Scanner
	file *os.File
	buffer bytes.Buffer
	seqIDs map[string]bool
}

/*Init fasta struct*/
func (i *FastaFile) Init(filename string) {
	(*i).filename = utils.Filename(filename)
	(*i).scanner, (*i).file = (*i).filename.ReturnReader(0)
	(*i).scanner.Scan()
	firstLine := (*i).scanner.Text()

	if firstLine[0] != '>' {
		panic(fmt.Sprintf("First line: %s from fasta file: %s doesn't have > fasta character",
			firstLine, (*i).filename))
	}

	(*i).currentSeqID = strings.SplitN(
		strings.Trim(firstLine, ">\n "), " ", 2)[0]

	(*i).buffer = bytes.Buffer{}
	_, (*i).genomename = path.Split(filename)
	split := strings.Split((*i).genomename, "_")

	if len(split) > 3 {
		(*i).genomename = fmt.Sprintf("%s_%s", split[0], split[1])
	}

	(*i).isInit = true
}

/*Close close fastq file */
func (i *FastaFile) Close() {
	utils.CloseFile((*i).file)
}

/*SaveCurrentSeqToBuffAndMove ... */
func (i *FastaFile) SaveCurrentSeqToBuffAndMove(savegenomename bool, match string) (eof bool) {
	(*i).buffer.WriteRune('>')
	(*i).buffer.WriteString((*i).currentSeqID)

	if savegenomename {
		(*i).buffer.WriteString("__")
		(*i).buffer.WriteString((*i).genomename)
	}

	if match != "" {
		(*i).buffer.WriteString("__")
		(*i).buffer.WriteString(match)
	}

	(*i).buffer.WriteRune('\n')

	eof = !(*i).scanner.Scan()
	currentLine := (*i).scanner.Text()

loop:
	for {
		switch {
		case eof:
			break loop
		case currentLine == "":
			eof = !(*i).scanner.Scan()
			currentLine = (*i).scanner.Text()

			continue loop
		case currentLine[0] == '>':
			break loop
		default:
			(*i).buffer.WriteString(currentLine)
			eof = !(*i).scanner.Scan()
			currentLine = (*i).scanner.Text()
		}
	}

	(*i).buffer.WriteRune('\n')

	if !eof {
		(*i).currentSeqID = strings.SplitN(
			strings.Trim(currentLine, ">\n "), " ", 2)[0]
	}

	return eof
}


/*SaveCurrentSeqIfCleantoBuffAndMove ... */
func (i *FastaFile) SaveCurrentSeqIfCleantoBuffAndMove(verbose bool) (
	eof bool, clean bool) {
	eof = !(*i).scanner.Scan()
	currentLine := (*i).scanner.Text()
	buffer := bytes.Buffer{}

loop:
	for {
		switch {
		case eof:
			break loop
		case currentLine == "":
			eof = !(*i).scanner.Scan()
			currentLine = (*i).scanner.Text()

			continue loop
		case currentLine[0] == '>':
			break loop
		default:
			buffer.WriteString(currentLine)
			eof = !(*i).scanner.Scan()
			currentLine = (*i).scanner.Text()
			clean = true
		}
	}

	switch clean {
	case true:
		(*i).buffer.WriteRune('>')
		(*i).buffer.WriteString((*i).currentSeqID)
		(*i).buffer.WriteRune('\n')
		(*i).buffer.Write(buffer.Bytes())
		(*i).buffer.WriteRune('\n')
	default:
		if verbose {
			fmt.Printf("Sequence empty: %s\n", (*i).currentSeqID)
		}
		os.Exit(1)
	}

	if !eof {
		(*i).currentSeqID = strings.SplitN(
			strings.Trim(currentLine, ">\n "), " ", 2)[0]
	}

	return eof, eof
}

/*WriteMatchingSeqIDToBuff write matching Seq ID to Buff */
func (i *FastaFile) WriteMatchingSeqIDToBuff(seqmap map[string]bool,
	savegenomename bool, trimSeqID bool) {
	var eof bool
	var currentSeqID string
	var split []string

	for !eof {
		currentSeqID = (*i).currentSeqID

		if trimSeqID {
			split = strings.Split(currentSeqID, "|")
			currentSeqID = split[len(split) - 1]
			currentSeqID = strings.Split(currentSeqID, "__")[0]

		}

		if seqmap[currentSeqID] {
			eof = (*i).SaveCurrentSeqToBuffAndMove(savegenomename, "")
		} else {
			eof = (*i).MovetoNextSeqID()
		}
	}

	utils.Check((*i).scanner.Err())
}

/*WriteMatchingSeqIDWithMatchToBuff write matching Seq ID to Buff with index in seqmap */
func (i *FastaFile) WriteMatchingSeqIDWithMatchToBuff(seqmap map[string]string,
	savegenomename bool, trimSeqID bool) {
	var eof bool
	var isInside bool
	var match string
	var currentSeqID string
	var split []string

	for !eof {
		currentSeqID = (*i).currentSeqID

		if trimSeqID {
			split = strings.Split(currentSeqID, "|")
			currentSeqID = split[len(split) - 1]
			currentSeqID = strings.Split(currentSeqID, "__")[0]
		}

		if match, isInside = seqmap[currentSeqID];isInside {
			eof = (*i).SaveCurrentSeqToBuffAndMove(savegenomename, match)
		} else {
			eof = (*i).MovetoNextSeqID()
		}
	}

	utils.Check((*i).scanner.Err())
}

/*WriteBuff write buffer and reset */
func (i *FastaFile) WriteBuff() []byte {
	defer (*i).buffer.Reset()

	return (*i).buffer.Bytes()
}

/*ScanForNbSeq write buffer and reset */
func (i *FastaFile) ScanForNbSeq() int {
	count := 0
	var eof bool

	for !eof {
		eof = (*i).MovetoNextSeqID()
		count++
	}

	utils.Check((*i).scanner.Err())
	(*i).Init(string((*i).filename))

	return count
}


/*GetSeqID  get seq ID */
func (i *FastaFile) GetSeqID() (seqID string) {
	seqID = (*i).currentSeqID
	return seqID
}

/*SetSeqID  set seq ID */
func (i *FastaFile) SetSeqID(seqID string) {
	(*i).currentSeqID = seqID
}


/*GetSeqIDAndMovetoNext ... */
func (i *FastaFile) GetSeqIDAndMovetoNext(trimSeqID bool) (eof bool, seqID string) {
	seqID = (*i).currentSeqID

	switch trimSeqID {
	case true:
		eof = (*i).MovetoNextSeqID()
	default:
		eof = (*i).MovetoNextSeqIDNoTrim()
	}

	return eof, seqID
}

/*GetSeqIDSeqSizeAndMovetoNext ... */
func (i *FastaFile) GetSeqIDSeqSizeAndMovetoNext() (eof bool, seqID string, size int) {
	seqID = (*i).currentSeqID

	eof, size = (*i).movetoNextSeqID(true)

	return eof, seqID, size
}


/*GetSeqIDSeqSizeSeqAndMovetoNext ... */
func (i *FastaFile) GetSeqIDSeqSizeSeqAndMovetoNext(trimName bool) (eof bool, seqID, seq string, size int) {
	seqID = (*i).currentSeqID

	eof, size, seq = (*i).movetoNextSeqIDAndReturnSeq(trimName)

	return eof, seqID,seq, size
}


/*GetAllSeqIDs get all seqIDS from fasta*/
func (i *FastaFile) GetAllSeqIDs(trimSeqID bool) (seqIDs map[string]bool) {

	if len((*i).seqIDs) != 0 {
		return (*i).seqIDs
	}

	eof := false
	var seqID string
	seqIDs = make(map[string]bool)


	for !eof {
		eof, seqID = (*i).GetSeqIDAndMovetoNext(trimSeqID)
		seqIDs[seqID] = true
	}

	(*i).seqIDs = seqIDs
	return seqIDs
}


/*MovetoNextSeqID Move to seq sequence ID > */
func (i *FastaFile) MovetoNextSeqID() (eof bool) {
	eof, _ = (*i).movetoNextSeqID(true)
	return eof
}

/*MovetoNextSeqIDNoTrim Move to seq sequence ID > */
func (i *FastaFile) MovetoNextSeqIDNoTrim() (eof bool) {
	eof, _ = (*i).movetoNextSeqID(false)
	return eof
}

/*MovetoNextSeqID Move to seq sequence ID > */
func (i *FastaFile) movetoNextSeqID(trimSeqID bool) (eof bool, seqsize int) {
	eof  = !(*i).scanner.Scan()

	if eof {
		return eof, 0
	}

	currentLine := (*i).scanner.Text()

loop:
	for {
		switch {
		case eof:
			seqsize += len(strings.Trim(currentLine, "\n"))
			return eof, seqsize
		case currentLine == "":
			seqsize += len(strings.Trim(currentLine, "\n"))
			eof = !(*i).scanner.Scan()
			currentLine = (*i).scanner.Text()
			continue loop

		case currentLine[0] == '>':
			break loop
		default:
			seqsize += len(strings.Trim(currentLine, "\n"))
			eof = !(*i).scanner.Scan()
			currentLine = (*i).scanner.Text()
		}
	}

	switch trimSeqID {
	case true:
		(*i).currentSeqID = strings.SplitN(
			strings.Trim(currentLine, ">\n "), " ", 2)[0]
	default:
		(*i).currentSeqID = strings.Trim(currentLine, ">\n ")
	}

	return eof, seqsize
}


/*MovetoNextSeqID Move to seq sequence ID > */
func (i *FastaFile) movetoNextSeqIDAndReturnSeq(trimSeqID bool) (eof bool, seqsize int, seq string) {
	eof  = !(*i).scanner.Scan()

	if eof {
		return eof, 0, seq
	}

	var trimmedLine string
	currentLine := (*i).scanner.Text()

loop:
	for {
		switch {
		case eof:
			trimmedLine = strings.Trim(currentLine, "\n")
			seqsize += len(trimmedLine)
			seq += trimmedLine
			return eof, seqsize, seq
		case currentLine == "":
			seqsize += len(strings.Trim(currentLine, "\n"))
			eof = !(*i).scanner.Scan()
			currentLine = (*i).scanner.Text()
			continue loop

		case currentLine[0] == '>':
			break loop
		default:
			trimmedLine = strings.Trim(currentLine, "\n")
			seqsize += len(trimmedLine)
			seq += trimmedLine
			eof = !(*i).scanner.Scan()
			currentLine = (*i).scanner.Text()
		}
	}

	switch trimSeqID {
	case true:
		(*i).currentSeqID = strings.SplitN(
			strings.Trim(currentLine, ">\n "), " ", 2)[0]
	default:
		(*i).currentSeqID = strings.Trim(currentLine, ">\n ")
	}

	return eof, seqsize, seq
}
