module gitlab.com/Grouumf/processfastafiles/fastaUtils

go 1.14

require (
	github.com/biogo/hts v1.4.3 // indirect
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/jinzhu/copier v0.3.2 // indirect
	github.com/klauspost/compress v1.13.3 // indirect
	github.com/klauspost/cpuid v1.3.1 // indirect
	github.com/mroth/weightedrand v0.4.1
	gitlab.com/Grouumf/ATACdemultiplex v0.48.2
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20210802220835-4b20ff939e6b
)
